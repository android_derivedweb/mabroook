package com.vouchers.mabroook.Adapter;

import static com.bumptech.glide.util.Preconditions.checkArgument;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vouchers.mabroook.Model.RaffleModel;
import com.vouchers.mabroook.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Adapter_our_winners extends RecyclerView.Adapter<Adapter_our_winners.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<RaffleModel> raffleModelArrayList;


    public Adapter_our_winners(Context mContext, ArrayList<RaffleModel> raffleModelArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.raffleModelArrayList = raffleModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_our_winners, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        DateFormat inputFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("MMM yyyy");
        DateFormat outputFormat1 = new SimpleDateFormat("dd");

        String inputDateStr = raffleModelArrayList.get(position).getDraw_datetime();

        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        String dateStr = getDayOfMonthSuffix(Integer.parseInt(outputFormat1.format(date)));

        holder.date.setText(outputFormat1.format(date) + "" + dateStr + " " + outputDateStr);


        holder.id_number.setText(raffleModelArrayList.get(position).getDraw_uid());

        holder.first_name.setText(raffleModelArrayList.get(position).getWinnersArrayList().get(0).getFirst_name());
        holder.second_name.setText(raffleModelArrayList.get(position).getWinnersArrayList().get(1).getFirst_name());
        holder.third_name.setText(raffleModelArrayList.get(position).getWinnersArrayList().get(2).getFirst_name());


    }

    @Override
    public int getItemCount() {
        return raffleModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView id_number, date, first_tour, second_tour, third_tour, first_name, second_name, third_name;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            id_number = itemView.findViewById(R.id.id_number);
            date = itemView.findViewById(R.id.date);
            first_tour = itemView.findViewById(R.id.first_tour);
            second_tour = itemView.findViewById(R.id.second_tour);
            third_tour = itemView.findViewById(R.id.third_tour);
            first_name = itemView.findViewById(R.id.first_name);
            second_name = itemView.findViewById(R.id.second_name);
            third_name = itemView.findViewById(R.id.third_name);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


    String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }


}

