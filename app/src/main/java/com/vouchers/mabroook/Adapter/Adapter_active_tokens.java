package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vouchers.mabroook.Model.ActivateModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_active_tokens extends RecyclerView.Adapter<Adapter_active_tokens.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ActivateModel> activateArrayList;


    public Adapter_active_tokens(Context mContext, ArrayList<ActivateModel> activateArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.activateArrayList = activateArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_active_tokens, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.brand_name.setText(activateArrayList.get(position).getBrand_name());
        holder.vouchers_count.setText(activateArrayList.get(position).getVoucher_cnt());


        Drawable sampleDrawable = mContext.getResources().getDrawable(R.drawable.round_red_10dp);
        sampleDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(activateArrayList.get(position).getBackground_color()), PorterDuff.Mode.MULTIPLY));
        holder.layout_bg.setBackground(sampleDrawable);

        Glide.with(mContext).load(activateArrayList.get(position).getBrand_logo()).into(holder.brand_logo);

    }

    @Override
    public int getItemCount() {
        return activateArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView vouchers_count, brand_name;
        ImageView brand_logo;
        LinearLayout layout_bg;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            vouchers_count = itemView.findViewById(R.id.vouchers_count);
            brand_name = itemView.findViewById(R.id.brand_name);
            brand_logo = itemView.findViewById(R.id.brand_logo);
            layout_bg = itemView.findViewById(R.id.layout_bg);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}

