package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vouchers.mabroook.Model.BrandModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_rewards_category extends RecyclerView.Adapter<Adapter_rewards_category.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private int selectedPosition = 0;

    private ArrayList<BrandModel> brandModelArrayList;


    public Adapter_rewards_category(Context mContext, ArrayList<BrandModel> brandModelArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.brandModelArrayList = brandModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_rewards_category, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onItemClick(position);

                if (selectedPosition >= 0) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = holder.getAdapterPosition();
                    notifyItemChanged(selectedPosition);
                }
            }
        });

        if (selectedPosition == position) {
            holder.itemView.setSelected(true); //using selector drawable
            holder.layout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_purple_5dp));

            holder.name.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.image.setColorFilter(mContext.getResources().getColor(R.color.white));
        } else {
            holder.itemView.setSelected(false);
            holder.layout.setBackground(mContext.getResources().getDrawable(R.drawable.round_white_8dp));

            holder.name.setTextColor(mContext.getResources().getColor(R.color.black1));
            holder.image.setColorFilter(mContext.getResources().getColor(R.color.black1));
        }


        holder.name.setText(brandModelArrayList.get(position).getBrand_category_name());

        Glide.with(mContext).load(brandModelArrayList.get(position).getBrand_category_icon_white()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return brandModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        LinearLayout layout;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            layout = itemView.findViewById(R.id.layout);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}

