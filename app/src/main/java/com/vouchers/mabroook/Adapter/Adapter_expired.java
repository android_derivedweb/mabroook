package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vouchers.mabroook.Model.RecentlyActivateModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_expired extends RecyclerView.Adapter<Adapter_expired.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<RecentlyActivateModel> expiredArrayList;


    public Adapter_expired(Context mContext, ArrayList<RecentlyActivateModel> expiredArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.expiredArrayList = expiredArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_expired, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        Glide.with(mContext).load(expiredArrayList.get(position).getBrand_logo()).into(holder.brand_logo);

        holder.ticketNo.setText("#" + expiredArrayList.get(position).getVoucher_uid());


    }

    @Override
    public int getItemCount() {
        return expiredArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView brand_logo;
        TextView ticketNo, draw_date_time;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            brand_logo = itemView.findViewById(R.id.brand_logo);
            ticketNo = itemView.findViewById(R.id.ticketNo);
            draw_date_time = itemView.findViewById(R.id.draw_date_time);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}

