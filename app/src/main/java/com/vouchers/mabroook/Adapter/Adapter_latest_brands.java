package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vouchers.mabroook.Model.BrandModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_latest_brands extends RecyclerView.Adapter<Adapter_latest_brands.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private ArrayList<BrandModel> latestBrandArrayList;

    public Adapter_latest_brands(Context mContext, ArrayList<BrandModel> latestBrandArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.latestBrandArrayList = latestBrandArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_latest_brands, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        Glide.with(mContext).load(latestBrandArrayList.get(position).getBrand_logo()).into(holder.brandIcon);
        Glide.with(mContext).load(latestBrandArrayList.get(position).getBackground_image()).into(holder.background_IV);

        holder.brand_name_TV.setText(latestBrandArrayList.get(position).getBrand_name());
        holder.tagline_TV.setText(latestBrandArrayList.get(position).getTag_line());

    }

    @Override
    public int getItemCount() {
        return latestBrandArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {


        ImageView brandIcon, background_IV;
        TextView brand_name_TV, tagline_TV;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            brandIcon = itemView.findViewById(R.id.brandIcon);
            background_IV = itemView.findViewById(R.id.background_IV);
            brand_name_TV = itemView.findViewById(R.id.brand_name_TV);
            tagline_TV = itemView.findViewById(R.id.tagline_TV);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}

