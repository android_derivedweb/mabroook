package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vouchers.mabroook.Model.BrandModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_search extends RecyclerView.Adapter<Adapter_search.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private ArrayList<BrandModel> arrayList;


    public Adapter_search(Context mContext, ArrayList<BrandModel> arrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_search, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.brand_name.setText(arrayList.get(position).getBrand_name());

        Glide.with(mContext).load(arrayList.get(position).getBrand_logo()).into(holder.brand_icon);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView brand_icon;
        TextView brand_name;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            brand_icon = itemView.findViewById(R.id.brand_icon);
            brand_name = itemView.findViewById(R.id.brand_name);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}

