package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Model.RecentlyActivateModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Adapter_recently_active_tokens extends RecyclerView.Adapter<Adapter_recently_active_tokens.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<RecentlyActivateModel> recentlyActivateArrayList;

    private UserSession session;


    public Adapter_recently_active_tokens(Context mContext, ArrayList<RecentlyActivateModel> recentlyActivateArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.recentlyActivateArrayList = recentlyActivateArrayList;

        session = new UserSession(mContext);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_recently_active_tokens, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        //        listener.onItemClick(position);

                if (recentlyActivateArrayList.get(position).getRaffle_status().equals("raffle")){
                    if (holder.layout_expand.getVisibility() == View.GONE) {
                        holder.layout_expand.setVisibility(View.VISIBLE);
                        holder.single_layout.setVisibility(View.GONE);
                    } else {
                        holder.layout_expand.setVisibility(View.GONE);
                        holder.single_layout.setVisibility(View.VISIBLE);
                    }
                }

            }
        });


        switch (recentlyActivateArrayList.get(position).getRaffle_status()) {
            case "raffle":

                holder.single_layout.setVisibility(View.VISIBLE);

                if (recentlyActivateArrayList.get(position).getRaffle_cnt().equals("1")) {
                    holder.info_text_single.setText(mContext.getString(R.string.single_raffle) + recentlyActivateArrayList.get(position).getActivation_date());
                } else {
                    holder.info_icon_single.setVisibility(View.VISIBLE);
                    holder.info_text_single.setText(R.string.multiple_raffle);
                }

                String[] nameArray = recentlyActivateArrayList.get(position).getMultiple_draw_name().split(",");
                StringBuilder sb = new StringBuilder();
                for (String s : nameArray) {
                    sb.append("☆" + s + "\n");
                }
                holder.draw_name_expand.setText(sb);

                String[] dateArray = recentlyActivateArrayList.get(position).getDraw_datetime().split(",");
                StringBuilder sb1 = new StringBuilder();
                for (String s : dateArray) {
                    sb1.append(s + "\n");
                }
                holder.draw_time_expand.setText(sb1);


                break;
            case "won":

                holder.layout_won.setVisibility(View.VISIBLE);

                break;
            case "lost":

                holder.layout_lost.setVisibility(View.VISIBLE);

                break;
            case "claimed":

                holder.layout_won.setVisibility(View.VISIBLE);
                holder.claim_text_won.setText(R.string.already_claimed_reward);
                holder.claim_reward_won.setVisibility(View.GONE);
                holder.claim_later_won.setVisibility(View.GONE);

                break;
        }


        holder.vouchers_uid_single.setText("#" + recentlyActivateArrayList.get(position).getVoucher_uid());
        holder.vouchers_uid_expand.setText("#" + recentlyActivateArrayList.get(position).getVoucher_uid());
        holder.vouchers_uid_lost.setText("#" + recentlyActivateArrayList.get(position).getVoucher_uid());
        holder.vouchers_uid_won.setText("#" + recentlyActivateArrayList.get(position).getVoucher_uid());

        holder.date_expand.setText(recentlyActivateArrayList.get(position).getActivation_date());
        holder.info_text_lost.setText(mContext.getString(R.string.raffle_was_on) + recentlyActivateArrayList.get(position).getActivation_date());
        holder.info_text_won.setText(mContext.getString(R.string.raffle_was_on) + recentlyActivateArrayList.get(position).getActivation_date());

        Glide.with(mContext).load(recentlyActivateArrayList.get(position).getBrand_logo()).into(holder.icon_single);
        Glide.with(mContext).load(recentlyActivateArrayList.get(position).getBrand_logo()).into(holder.icon_expand);
        Glide.with(mContext).load(recentlyActivateArrayList.get(position).getBrand_logo()).into(holder.icon_lost);


        holder.claim_reward_won.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rewardClaim(recentlyActivateArrayList.get(position).getRaffle_id(), holder);
            }
        });


    }

    @Override
    public int getItemCount() {
        return recentlyActivateArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        private CardView single_layout, layout_expand, layout_lost;
        private LinearLayout layout_won;

        private ImageView info_icon_single, icon_single, icon_expand, icon_lost;
        private TextView vouchers_uid_single, vouchers_uid_expand, vouchers_uid_lost, vouchers_uid_won, claim_reward_won, claim_later_won, claim_text_won;
        private TextView info_text_single, date_expand, draw_name_expand, draw_time_expand, info_text_lost, info_text_won;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            single_layout = itemView.findViewById(R.id.single_layout);
            layout_expand = itemView.findViewById(R.id.layout_expand);
            layout_lost = itemView.findViewById(R.id.layout_lost);
            layout_won = itemView.findViewById(R.id.layout_won);

            info_icon_single = itemView.findViewById(R.id.info_icon_single);
            icon_single = itemView.findViewById(R.id.icon_single);
            icon_expand = itemView.findViewById(R.id.icon_expand);
            icon_lost = itemView.findViewById(R.id.icon_lost);

            vouchers_uid_single = itemView.findViewById(R.id.vouchers_uid_single);
            vouchers_uid_expand = itemView.findViewById(R.id.vouchers_uid_expand);
            vouchers_uid_lost = itemView.findViewById(R.id.vouchers_uid_lost);
            vouchers_uid_won = itemView.findViewById(R.id.vouchers_uid_won);
            claim_reward_won = itemView.findViewById(R.id.claim_reward_won);
            claim_later_won = itemView.findViewById(R.id.claim_later_won);
            claim_text_won = itemView.findViewById(R.id.claim_text_won);

            info_text_single = itemView.findViewById(R.id.info_text_single);
            date_expand = itemView.findViewById(R.id.date_expand);
            draw_name_expand = itemView.findViewById(R.id.draw_name_expand);
            draw_time_expand = itemView.findViewById(R.id.draw_time_expand);
            info_text_lost = itemView.findViewById(R.id.info_text_lost);
            info_text_won = itemView.findViewById(R.id.info_text_won);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClaimWon(int item);
    }



    private void rewardClaim(String raffle_id, Viewholder holder) {
        KProgressHUD progressDialog = KProgressHUD.create(mContext)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "claim-my-reward?raffle_id=" + raffle_id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("claim_reward", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                holder.claim_text_won.setText(R.string.already_claimed_reward);
                                holder.claim_reward_won.setVisibility(View.GONE);
                                holder.claim_later_won.setVisibility(View.GONE);

                                Toast.makeText(mContext, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(mContext, json1, Toast.LENGTH_LONG).show();
                                mContext.startActivity(new Intent(mContext, Login_SignUp.class));
                    //            getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(mContext, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(mContext, "Bad Network Connection", Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(mContext).add(volleyMultipartRequest);
    }



}

