package com.vouchers.mabroook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vouchers.mabroook.Model.CityModel;
import com.vouchers.mabroook.Model.RecentlyActivateModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_brand extends RecyclerView.Adapter<Adapter_brand.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private Adapter_recently_active_tokens adapter_recently_active_tokens;

    private ArrayList<CityModel> brandArrayList;


    public Adapter_brand(Context mContext, ArrayList<CityModel> brandArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.brandArrayList = brandArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_brand, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.brand_name.setText(brandArrayList.get(position).getName());


        holder.rec_Recently_Active_Tokens.setLayoutManager(new LinearLayoutManager(mContext));
        adapter_recently_active_tokens = new Adapter_recently_active_tokens(mContext, brandArrayList.get(position).getRecentlyActivateArrayList(), new Adapter_recently_active_tokens.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }

            @Override
            public void onItemClaimWon(int item) {

            }
        });
        holder.rec_Recently_Active_Tokens.setAdapter(adapter_recently_active_tokens);


    }

    @Override
    public int getItemCount() {
        return brandArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        private RecyclerView rec_Recently_Active_Tokens;
        private TextView brand_name;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            rec_Recently_Active_Tokens = itemView.findViewById(R.id.rec_Recently_Active_Tokens);
            brand_name = itemView.findViewById(R.id.brand_name);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}

