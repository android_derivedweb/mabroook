package com.vouchers.mabroook.Adapter;

import static com.bumptech.glide.util.Preconditions.checkArgument;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vouchers.mabroook.Model.WinnersModel;
import com.vouchers.mabroook.R;

import java.util.ArrayList;

public class Adapter_winners extends RecyclerView.Adapter<Adapter_winners.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<WinnersModel> winnersModelArrayList;


    public Adapter_winners(Context mContext, ArrayList<WinnersModel> winnersModelArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.listener = onItemClickListener;
        this.winnersModelArrayList = winnersModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_winners, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.name.setText(winnersModelArrayList.get(position).getFirst_name());
        holder.orderTV.setText(winnersModelArrayList.get(position).getPrize_number()
                + getDayOfMonthSuffix(Integer.parseInt(winnersModelArrayList.get(position).getPrize_number())));


    }

    @Override
    public int getItemCount() {
        return winnersModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView name, orderTV;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            orderTV = itemView.findViewById(R.id.orderTV);
            name = itemView.findViewById(R.id.name);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

    String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }

}

