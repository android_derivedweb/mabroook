package com.vouchers.mabroook.Model;


public class WinnersModel {

    private String raffle_uid;
    private String prize_title;
    private String first_name;
    private String prize_number;


    public String getRaffle_uid() {
        return raffle_uid;
    }

    public void setRaffle_uid(String raffle_uid) {
        this.raffle_uid = raffle_uid;
    }

    public String getPrize_title() {
        return prize_title;
    }

    public void setPrize_title(String prize_title) {
        this.prize_title = prize_title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPrize_number() {
        return prize_number;
    }

    public void setPrize_number(String prize_number) {
        this.prize_number = prize_number;
    }


}
