package com.vouchers.mabroook.Model;


import java.util.ArrayList;

public class RaffleModel {

    private String draw_id;
    private String recurring_draw_id;
    private String draw_uid;
    private String draw_name;
    private String draw_type;
    private String status;
    private String draw_datetime;

    private ArrayList<WinnersModel> winnersArrayList;

    public String getDraw_datetime() {
        return draw_datetime;
    }

    public void setDraw_datetime(String draw_datetime) {
        this.draw_datetime = draw_datetime;
    }

    public String getDraw_id() {
        return draw_id;
    }

    public void setDraw_id(String draw_id) {
        this.draw_id = draw_id;
    }

    public String getRecurring_draw_id() {
        return recurring_draw_id;
    }

    public void setRecurring_draw_id(String recurring_draw_id) {
        this.recurring_draw_id = recurring_draw_id;
    }

    public String getDraw_uid() {
        return draw_uid;
    }

    public void setDraw_uid(String draw_uid) {
        this.draw_uid = draw_uid;
    }

    public String getDraw_name() {
        return draw_name;
    }

    public void setDraw_name(String draw_name) {
        this.draw_name = draw_name;
    }

    public String getDraw_type() {
        return draw_type;
    }

    public void setDraw_type(String draw_type) {
        this.draw_type = draw_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<WinnersModel> getWinnersArrayList() {
        return winnersArrayList;
    }

    public void setWinnersArrayList(ArrayList<WinnersModel> winnersArrayList) {
        this.winnersArrayList = winnersArrayList;
    }
}
