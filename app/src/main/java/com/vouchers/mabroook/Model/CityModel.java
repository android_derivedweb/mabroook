package com.vouchers.mabroook.Model;


import java.util.ArrayList;

public class CityModel {

    private String name;
    private String id;

    private ArrayList<RecentlyActivateModel> recentlyActivateArrayList = new ArrayList<>();


    public ArrayList<RecentlyActivateModel> getRecentlyActivateArrayList() {
        return recentlyActivateArrayList;
    }

    public void setRecentlyActivateArrayList(ArrayList<RecentlyActivateModel> recentlyActivateArrayList) {
        this.recentlyActivateArrayList = recentlyActivateArrayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
