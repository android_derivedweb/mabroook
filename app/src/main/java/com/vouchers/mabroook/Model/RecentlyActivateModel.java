package com.vouchers.mabroook.Model;


public class RecentlyActivateModel {

    private String voucher_id;
    private String raffle_id;
    private String voucher_uid;
    private String raffle_uid;
    private String brand_logo;
    private String raffle_status;
    private String activation_date;
    private String multiple_draw_name;
    private String raffle_cnt;
    private String draw_datetime;
    private String voucher_status;


    public String getVoucher_id() {
        return voucher_id;
    }

    public void setVoucher_id(String voucher_id) {
        this.voucher_id = voucher_id;
    }

    public String getRaffle_id() {
        return raffle_id;
    }

    public void setRaffle_id(String raffle_id) {
        this.raffle_id = raffle_id;
    }

    public String getVoucher_uid() {
        return voucher_uid;
    }

    public void setVoucher_uid(String voucher_uid) {
        this.voucher_uid = voucher_uid;
    }

    public String getRaffle_uid() {
        return raffle_uid;
    }

    public void setRaffle_uid(String raffle_uid) {
        this.raffle_uid = raffle_uid;
    }

    public String getBrand_logo() {
        return brand_logo;
    }

    public void setBrand_logo(String brand_logo) {
        this.brand_logo = brand_logo;
    }

    public String getRaffle_status() {
        return raffle_status;
    }

    public void setRaffle_status(String raffle_status) {
        this.raffle_status = raffle_status;
    }

    public String getActivation_date() {
        return activation_date;
    }

    public void setActivation_date(String activation_date) {
        this.activation_date = activation_date;
    }

    public String getMultiple_draw_name() {
        return multiple_draw_name;
    }

    public void setMultiple_draw_name(String multiple_draw_name) {
        this.multiple_draw_name = multiple_draw_name;
    }

    public String getRaffle_cnt() {
        return raffle_cnt;
    }

    public void setRaffle_cnt(String raffle_cnt) {
        this.raffle_cnt = raffle_cnt;
    }

    public String getDraw_datetime() {
        return draw_datetime;
    }

    public void setDraw_datetime(String draw_datetime) {
        this.draw_datetime = draw_datetime;
    }

    public String getVoucher_status() {
        return voucher_status;
    }

    public void setVoucher_status(String voucher_status) {
        this.voucher_status = voucher_status;
    }
}
