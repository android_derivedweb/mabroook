package com.vouchers.mabroook.Model;


public class BrandModel {

    private String brand_category_id;
    private String brand_category_name;
    private String brand_category_icon_white;

    private String brand_id;
    private String brand_logo;
    private String background_image;
    private String brand_name;
    private String tag_line;

    public String getTag_line() {
        return tag_line;
    }

    public void setTag_line(String tag_line) {
        this.tag_line = tag_line;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_logo() {
        return brand_logo;
    }

    public void setBrand_logo(String brand_logo) {
        this.brand_logo = brand_logo;
    }

    public String getBackground_image() {
        return background_image;
    }

    public void setBackground_image(String background_image) {
        this.background_image = background_image;
    }

    public String getBrand_category_id() {
        return brand_category_id;
    }

    public void setBrand_category_id(String brand_category_id) {
        this.brand_category_id = brand_category_id;
    }

    public String getBrand_category_name() {
        return brand_category_name;
    }

    public void setBrand_category_name(String brand_category_name) {
        this.brand_category_name = brand_category_name;
    }

    public String getBrand_category_icon_white() {
        return brand_category_icon_white;
    }

    public void setBrand_category_icon_white(String brand_category_icon_white) {
        this.brand_category_icon_white = brand_category_icon_white;
    }


}
