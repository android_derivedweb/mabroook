package com.vouchers.mabroook.Model;


public class ActivateModel {

    private String brand_id;
    private String partner_name;
    private String brand_logo;
    private String brand_name;
    private String background_color;
    private String voucher_cnt;
    private String partner_id;

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getBrand_logo() {
        return brand_logo;
    }

    public void setBrand_logo(String brand_logo) {
        this.brand_logo = brand_logo;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public String getVoucher_cnt() {
        return voucher_cnt;
    }

    public void setVoucher_cnt(String voucher_cnt) {
        this.voucher_cnt = voucher_cnt;
    }
}
