package com.vouchers.mabroook.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public String BASEURL = "https://chessmafia.com/php/mabrook/api/";

    private static final String PREF_NAME = "UserSessionPref";


    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";
    private static final String CITY_ID = "city_id";
    private static final String STATE_ID = "state_id";
    private static final String COUNTRY_ID = "country_id";
    private static final String POST_CODE = "postCode";


    private final String USER_ID = "User_id";
    private final String FIRSTNAME = "first_name";
    private final String LASTNAME = "last_name";
    private final String EMAIL = "email";
    private final String PHONE = "phone";
    private final String PROFILE_PIC = "profile_pic";
    private final String FACEBOOK_ID = "facebook_id";
    private final String GOOGLE_ID = "google_id";
    private final String LANGUAGE = "language";
    private final String ADDRESS_LINE_1 = "address_line_1";
    private final String ADDRESS_LINE_2 = "address_line_2";
    private final String DOB = "dob";
    private final String STATUS = "status";
    private final String DEVICE_TOKEN = "device_token";

    private final String APITOKEN = "api_token";


    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }


    public void createLoginSession(String user_id,
                                   String first_name,
                                   String last_name,
                                   String email,
                                   String phone,
                                   String postcode,
                                   String api_token
    ) {
        editor.putString(USER_ID, user_id);
        editor.putString(FIRSTNAME, first_name);
        editor.putString(LASTNAME, last_name);
        editor.putString(EMAIL, email);
        editor.putString(PHONE, phone);
        editor.putString(POST_CODE, postcode);
        editor.putString(APITOKEN, api_token);
        editor.commit();
    }




    public void setIsLogin(boolean b) {
        editor.putBoolean(IS_LOGIN, b);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }


    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }

    public void setLatitude(String latitude) {
        editor.putString(LATITUDE, latitude);
        editor.commit();
    }


    public String getLatitude() {
        return sharedPreferences.getString(LATITUDE, "");
    }


    public void setPROFILE_PIC(String profile_pic) {
        editor.putString(PROFILE_PIC, profile_pic);
        editor.commit();
    }


    public String getPROFILE_PIC() {
        return sharedPreferences.getString(PROFILE_PIC, "");
    }

    public void setFACEBOOK_ID(String facebook_id) {
        editor.putString(FACEBOOK_ID, facebook_id);
        editor.commit();
    }


    public String getFACEBOOK_ID() {
        return sharedPreferences.getString(FACEBOOK_ID, "");
    }

    public void setGOOGLE_ID(String google_id) {
        editor.putString(GOOGLE_ID, google_id);
        editor.commit();
    }


    public String getGOOGLE_ID() {
        return sharedPreferences.getString(GOOGLE_ID, "");
    }

    public void setLANGUAGE(String language) {
        editor.putString(LANGUAGE, language);
        editor.commit();
    }


    public String getLANGUAGE() {
        return sharedPreferences.getString(LANGUAGE, "");
    }

    public void setADDRESS_LINE_1(String address_line_1) {
        editor.putString(ADDRESS_LINE_1, address_line_1);
        editor.commit();
    }


    public String getADDRESS_LINE_1() {
        return sharedPreferences.getString(ADDRESS_LINE_1, "");
    }

    public void setADDRESS_LINE_2(String address_line_2) {
        editor.commit();
        editor.putString(ADDRESS_LINE_2, address_line_2);
    }


    public String getADDRESS_LINE_2() {
        return sharedPreferences.getString(ADDRESS_LINE_2, "");
    }


    public void setDOB(String dob) {
        editor.putString(DOB, dob);
        editor.commit();
    }


    public String getDOB() {
        return sharedPreferences.getString(DOB, "");
    }

    public void setSTATUS(String status) {
        editor.putString(STATUS, status);
        editor.commit();
    }


    public String getSTATUS() {
        return sharedPreferences.getString(STATUS, "");
    }

    public void setDEVICE_TOKEN(String device_token) {
        editor.putString(DEVICE_TOKEN, device_token);
        editor.commit();
    }


    public String getDEVICE_TOKEN() {
        return sharedPreferences.getString(DEVICE_TOKEN, "");
    }



    public void setLongitude(String longitude) {
        editor.putString(LONGITUDE, longitude);
        editor.commit();
    }

    public String getLongitude() {
        return sharedPreferences.getString(LONGITUDE, "");
    }


    public void setCityId(String cityId) {
        editor.putString(CITY_ID, cityId);
        editor.commit();
    }

    public String getCityId() {
        return sharedPreferences.getString(CITY_ID, "");
    }

    public void setStateId(String stateId) {
        editor.putString(STATE_ID, stateId);
        editor.commit();
    }

    public String getStateId() {
        return sharedPreferences.getString(STATE_ID, "");
    }

    public void setCountryId(String countryId) {
        editor.putString(COUNTRY_ID, countryId);
        editor.commit();
    }

    public String getCountryId() {
        return sharedPreferences.getString(COUNTRY_ID, "");
    }

    public void setPostCode(String postCode) {
        editor.putString(POST_CODE, postCode);
        editor.commit();
    }

    public String getPostCode() {
        return sharedPreferences.getString(POST_CODE, "");
    }


    public void setUSER_ID(String user_id) {
        editor.putString(USER_ID, user_id);
        editor.commit();
    }

    public String getUSER_ID() {
        return sharedPreferences.getString(USER_ID, "");
    }

    public void setFIRSTNAME(String firstname) {
        editor.putString(FIRSTNAME, firstname);
        editor.commit();
    }

    public String getFIRSTNAME() {
        return sharedPreferences.getString(FIRSTNAME, "");
    }

    public void setLASTNAME(String lastname) {
        editor.putString(LASTNAME, lastname);
        editor.commit();
    }

    public String getLASTNAME() {
        return sharedPreferences.getString(LASTNAME, "");
    }

    public void setEMAIL(String email) {
        editor.putString(EMAIL, email);
        editor.commit();
    }

    public String getEMAIL() {
        return sharedPreferences.getString(EMAIL, "");
    }



    public void setPHONE(String phone) {
        editor.putString(PHONE, phone);
        editor.commit();
    }

    public String getPHONE() {
        return sharedPreferences.getString(PHONE, "");
    }


    public void setAPITOKEN(String apitoken) {
        editor.putString(APITOKEN, apitoken);
        editor.commit();
    }

    public String getAPITOKEN() {
        return sharedPreferences.getString(APITOKEN, "");
    }



    public String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }


}
