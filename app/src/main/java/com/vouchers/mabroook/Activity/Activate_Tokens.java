package com.vouchers.mabroook.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Model.ActivateModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activate_Tokens extends AppCompatActivity {

    private TextView number_token, available_tokens;
    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_tokens);

        session = new UserSession(Activate_Tokens.this);


        number_token = findViewById(R.id.number_token);
        available_tokens = findViewById(R.id.available_tokens);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        number_token.setText(getIntent().getStringExtra("voucher_cnt"));
        available_tokens.setText("Available tokens : " + getIntent().getStringExtra("voucher_cnt"));


        findViewById(R.id.decrement).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Integer.parseInt(number_token.getText().toString()) > 1) {
                    int token_count = Integer.parseInt(number_token.getText().toString());
                    token_count--;
                    number_token.setText(String.valueOf(token_count));
                }
            }
        });

        findViewById(R.id.increment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Integer.parseInt(number_token.getText().toString()) < Integer.parseInt(getIntent().getStringExtra("voucher_cnt"))) {
                    int token_count = Integer.parseInt(number_token.getText().toString());
                    token_count++;
                    number_token.setText(String.valueOf(token_count));
                }

            }
        });


        findViewById(R.id.activate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_activate_tokens();
            }
        });


    }
    
    
    private void set_activate_tokens() {
        KProgressHUD progressDialog = KProgressHUD.create(Activate_Tokens.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "activate-token",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("activate_tokens", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                Toast.makeText(Activate_Tokens.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Activate_Tokens.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(Activate_Tokens.this, json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Activate_Tokens.this, Login_SignUp.class));
                                finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(Activate_Tokens.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Activate_Tokens.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("partner_id", getIntent().getStringExtra("partner_id"));
                params.put("avail_voucher", number_token.getText().toString());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Activate_Tokens.this).add(volleyMultipartRequest);
    }

    
}