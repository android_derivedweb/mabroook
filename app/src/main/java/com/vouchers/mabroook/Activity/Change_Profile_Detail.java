package com.vouchers.mabroook.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login;
import com.vouchers.mabroook.Adapter.SelectCitySpinner;
import com.vouchers.mabroook.Model.CityModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Change_Profile_Detail extends AppCompatActivity {

    private UserSession session;

    private static TextView dob_tv;

    private ImageView image_dialog;
    private Bitmap profile_bitmap = null;

    private File final_path = null;

    private TextInputEditText first_name, surname, email, phone;
    private TextInputEditText id_number, address_line_1, address_line_2, postcode;

    private Spinner countrySpinner, stateSpinner, citySpinner;
    private RelativeLayout state_layout, city_layout;

    private ArrayList<CityModel> country_ArrayList = new ArrayList<>();
    private ArrayList<CityModel> state_ArrayList = new ArrayList<>();
    private ArrayList<CityModel> city_ArrayList = new ArrayList<>();

    private String country_id = "";
    private String state_id = "";
    private String city_id = "";

    private boolean isCountrySelect = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile_detail);

        session = new UserSession(Change_Profile_Detail.this);

        image_dialog = findViewById(R.id.image_dialog);
        dob_tv = findViewById(R.id.dob_tv);
        first_name = findViewById(R.id.first_name);
        surname = findViewById(R.id.surname);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        id_number = findViewById(R.id.id_number);
        address_line_1 = findViewById(R.id.address_line_1);
        address_line_2 = findViewById(R.id.address_line_2);
        postcode = findViewById(R.id.postcode);

        countrySpinner = findViewById(R.id.countrySpinner);
        stateSpinner = findViewById(R.id.stateSpinner);
        citySpinner = findViewById(R.id.citySpinner);

        state_layout = findViewById(R.id.state_layout);
        city_layout = findViewById(R.id.city_layout);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        image_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        dob_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePickerGraduation");
            }
        });

        
        findViewById(R.id.signUpBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (first_name.getText().toString().isEmpty()) {
                    Toast.makeText(Change_Profile_Detail.this, R.string.enter_first_name, Toast.LENGTH_SHORT).show();
                } else if (surname.getText().toString().isEmpty()){
                    Toast.makeText(Change_Profile_Detail.this, R.string.enter_your_surname, Toast.LENGTH_SHORT).show();
                } else {
                    updateProfile();
                    Log.e("cechhiDS", country_id + "--" + state_id + "--" + city_id);
                }
                
            }
        });

        getProfile();

    }


    private void getProfile() {
        final KProgressHUD progressDialog = KProgressHUD.create(Change_Profile_Detail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "edit-my-profile", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                progressDialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(new String(response.data));
                    Log.e("getProfileResponse", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        JSONObject object = jsonObject.getJSONObject("data").getJSONObject("user");

                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        Glide.with(Change_Profile_Detail.this).load(object.getString("profile_pic")).into(image_dialog);

                        profile_bitmap = getBitmapFromURL(object.getString("profile_pic"));


                        first_name.setText(object.getString("first_name"));
                        surname.setText(object.getString("last_name"));
                        dob_tv.setText(object.getString("dob"));


                        if (!object.getString("email").equals("null")){
                            email.setText(object.getString("email"));
                        }
                        if (!object.getString("phone").equals("null")){
                            phone.setText(object.getString("phone"));
                        }
                        email.setEnabled(false);
                        phone.setEnabled(false);


                        if (!object.getString("id_number").equals("null")){
                            id_number.setText(object.getString("id_number"));
                        }
                        if (!object.getString("address_line_1").equals("null")){
                            address_line_1.setText(object.getString("address_line_1"));
                        }
                        if (!object.getString("address_line_2").equals("null")){
                            address_line_2.setText(object.getString("address_line_2"));
                        }
                        if (!object.getString("postcode").equals("null")){
                            postcode.setText(object.getString("postcode"));
                        }



                        if (!object.getString("country_id").equals("null")){
                            country_id = object.getString("country_id");
                        }
                        if (!object.getString("state_id").equals("null")){
                            state_id = object.getString("state_id");
                        }
                        if (!object.getString("city_id").equals("null")){
                            city_id = object.getString("city_id");
                        }

                        getCountry();



                        Log.e("checkIds", country_id + "--" + state_id + "--" + city_id);


                    } else if (jsonObject.getString("ResponseCode").equals("401")) {

                        session.logout();
                        Intent intent = new Intent(Change_Profile_Detail.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }

                } catch (Exception e) {
                    Toast.makeText(Change_Profile_Detail.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        /*session.logout();
                        Intent intent = new Intent(getActivity(), Activity_SelectCity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();*/

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_Profile_Detail.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(Change_Profile_Detail.this).add(volleyMultipartRequest);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }



    private void updateProfile() {
        final KProgressHUD progressDialog = KProgressHUD.create(Change_Profile_Detail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "update-my-profile", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                progressDialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(new String(response.data));
                    Log.e("ResponseUpdateProfile", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        Toast.makeText(Change_Profile_Detail.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                        finish();

                    } else if (jsonObject.getString("ResponseCode").equals("401")) {

                        session.logout();
                        Intent intent = new Intent(Change_Profile_Detail.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }

                } catch (Exception e) {
                    Toast.makeText(Change_Profile_Detail.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        /*session.logout();
                        Intent intent = new Intent(getActivity(), Activity_SelectCity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();*/

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_Profile_Detail.this, json, Toast.LENGTH_LONG).show();
                                            Log.e("error_update_profile", json + "");
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("first_name", first_name.getText().toString());
                params.put("last_name", surname.getText().toString());
                params.put("dob", dob_tv.getText().toString());
                params.put("id_number", id_number.getText().toString());
                params.put("address_line_1", address_line_1.getText().toString());
                params.put("address_line_2", address_line_2.getText().toString());
                params.put("country_id", country_id);
                params.put("state_id", state_id);
                params.put("city_id", city_id);
                params.put("postcode", postcode.getText().toString());

                Log.e("checkUpdateData", country_id + "--" + state_id + "--" + city_id);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("profile_image", new DataPart(imagename + ".png", getFileDataFromDrawable(profile_bitmap)));
                return params;
            }
        };
        Volley.newRequestQueue(Change_Profile_Detail.this).add(volleyMultipartRequest);

    }


    private void getCountry() {
        KProgressHUD progressDialog = KProgressHUD.create(Change_Profile_Detail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
            //    .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-countries",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        country_ArrayList.clear();

                        Log.e("responseCountries", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("countries");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel model = new CityModel();
                                        model.setId(object.getString("country_id"));
                                        model.setName(object.getString("country_name"));
                                        country_ArrayList.add(model);
                                    }

                                    CityModel model = new CityModel();
                                    model.setId("");
                                    model.setName(getString(R.string.select_country));
                                    country_ArrayList.add(model);

                                    SelectCitySpinner adapter1 = new SelectCitySpinner(Change_Profile_Detail.this, android.R.layout.simple_spinner_item, country_ArrayList);
                                    adapter1.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                    countrySpinner.setAdapter(adapter1);
                                    if (!country_id.equals("null")){
                                        for (int i = 0; i < country_ArrayList.size(); i++){
                                            if (country_id.equals(country_ArrayList.get(i).getId())){
                                                countrySpinner.setSelection(i);
                                            }
                                        }
                                    }

                                    countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            state_layout.setVisibility(View.VISIBLE);
                                            city_layout.setVisibility(View.GONE);
                                            if (isCountrySelect) {
                                                city_id = "";
                                            }
                                            isCountrySelect = true;

                                            getState(country_ArrayList.get(position).getId());
                                            country_id = country_ArrayList.get(position).getId();
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });

                                    getState(country_id);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Change_Profile_Detail.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Change_Profile_Detail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_Profile_Detail.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Change_Profile_Detail.this).add(volleyMultipartRequest);
    }


    private void getState(String country_id) {
        KProgressHUD progressDialog = KProgressHUD.create(Change_Profile_Detail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
          //      .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-states?country_id=" + country_id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        state_ArrayList.clear();

                        Log.e("responseState", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("states");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel model = new CityModel();
                                        model.setId(object.getString("state_id"));
                                        model.setName(object.getString("state_name"));
                                        state_ArrayList.add(model);
                                    }

                                    CityModel model = new CityModel();
                                    model.setId("");
                                    model.setName(getString(R.string.select_state));
                                    state_ArrayList.add(model);

                                    SelectCitySpinner adapter = new SelectCitySpinner(Change_Profile_Detail.this, android.R.layout.simple_spinner_item, state_ArrayList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                    stateSpinner.setAdapter(adapter);
                                    if (!state_id.equals("null")){
                                        for (int i = 0; i < state_ArrayList.size(); i++){
                                            if (state_id.equals(state_ArrayList.get(i).getId())){
                                                stateSpinner.setSelection(i);
                                            }
                                        }
                                    } else {
                                        state_layout.setVisibility(View.GONE);
                                        state_id = "";
                                    }

                                    stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            city_layout.setVisibility(View.VISIBLE);
                                            getCity(country_id, state_ArrayList.get(position).getId());
                                            state_id = state_ArrayList.get(position).getId();
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });

                                    if (state_ArrayList.size() < 2){
                                        state_layout.setVisibility(View.GONE);
                                        state_id = "";
                                    }

                                    getCity(country_id, state_id);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Change_Profile_Detail.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Change_Profile_Detail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_Profile_Detail.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Change_Profile_Detail.this).add(volleyMultipartRequest);
    }


    private void getCity(String country_id, String state_id) {
        KProgressHUD progressDialog = KProgressHUD.create(Change_Profile_Detail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        Log.e("dsdsds", country_id + "--" + state_id);
         //       .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "get-cities?country_id=" + country_id + "&state_id=" + state_id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        city_ArrayList.clear();

                        Log.e("responseCity", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("cities");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel model = new CityModel();
                                        model.setId(object.getString("city_id"));
                                        model.setName(object.getString("city_name"));
                                        city_ArrayList.add(model);
                                    }

                                    CityModel model = new CityModel();
                                    model.setId("");
                                    model.setName(getString(R.string.select_city));
                                    city_ArrayList.add(model);

                                    SelectCitySpinner adapter11 = new SelectCitySpinner(Change_Profile_Detail.this, android.R.layout.simple_spinner_item, city_ArrayList);
                                    adapter11.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                    citySpinner.setAdapter(adapter11);
                                    if (!city_id.equals("null")){
                                        for (int i = 0; i < city_ArrayList.size(); i++){
                                            if (city_id.equals(city_ArrayList.get(i).getId())){
                                                citySpinner.setSelection(i);
                                            }
                                        }
                                    } else {
                                        city_layout.setVisibility(View.GONE);
                                        city_id = "";
                                    }

                                    citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            city_id = city_ArrayList.get(position).getId();
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });

                                    if (city_ArrayList.size() < 2){
                                        city_layout.setVisibility(View.GONE);
                                        city_id = "";
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Change_Profile_Detail.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Change_Profile_Detail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_Profile_Detail.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_Profile_Detail.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Change_Profile_Detail.this).add(volleyMultipartRequest);
    }



    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return  dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            dob_tv.setText(dateString);

           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }
    }


    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_from_gallery), getString(R.string.cancel)};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Change_Profile_Detail.this);
                builder.setTitle(R.string.select_option);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getResources().getString(R.string.take_photo))) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else {
                checkAndRequestPermissions();
                //  Toast.makeText(dialogForCity.getContext(), "Permission error", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            checkAndRequestPermissions();
            Toast.makeText(Change_Profile_Detail.this, R.string.permission_error, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(Change_Profile_Detail.this,
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(Change_Profile_Detail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(Change_Profile_Detail.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(Change_Profile_Detail.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("in fragment on request", "Permission callback called-------");

        switch (requestCode) {

            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    Log.e("Permision_done","Permision_done");
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");

                        Log.e("Permision_done","Permision_done");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Change_Profile_Detail.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(Change_Profile_Detail.this, Manifest.permission.CAMERA) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(Change_Profile_Detail.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK(getString(R.string.camera_storegae_required),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(Change_Profile_Detail.this, R.string.go_to_setting_permission, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                }
            }
        }

    }


    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Change_Profile_Detail.this)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setNegativeButton(R.string.cancel, okListener)
                .create()
                .show();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();

                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                File folder = new File(Change_Profile_Detail.this.getExternalFilesDir(null) + "/" + "SNAGpay");

                if (!folder.exists()) {
                    folder.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                File destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        "SNAGpay", "HP_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String imgPath = destination.getAbsolutePath();
                profile_bitmap = bitmap;
                final_path = new File(Environment.getExternalStorageDirectory() + "/" +
                        "SNAGpay", "IMG_" + timeStamp + ".jpg");
                Glide.with(Change_Profile_Detail.this).asBitmap().load(bitmap).circleCrop().into(image_dialog);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data != null) {
                try {


                    Uri selectedImageUri = data.getData();
                    String selectedImagePath = getRealPathFromURI(selectedImageUri);
                    Log.e("selectedImagePath", selectedImagePath);

                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    final Bitmap imagebitmap = BitmapFactory.decodeStream(imageStream);

                    String path = getPath(selectedImageUri);
                    Matrix matrix = new Matrix();
                    ExifInterface exif = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        try {
                            exif = new ExifInterface(path);
                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.postRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.postRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.postRotate(270);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    Bitmap bitmap = Bitmap.createBitmap(imagebitmap, 0, 0, imagebitmap.getWidth(), imagebitmap.getHeight(), matrix, true);


                    String imgPath = getRealPathFromURI(selectedImageUri);
                    File destination = new File(imgPath.toString());
                    //  txt_injury.setText(imgPath);

                    profile_bitmap = bitmap;
                    final_path = new File(imgPath.toString());
                    Glide.with(Change_Profile_Detail.this).asBitmap().load(bitmap).circleCrop().into(image_dialog);



                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    }


    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getPath(Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = getString(R.string.not_found);
        }
        return result;
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    
}