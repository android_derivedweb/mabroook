package com.vouchers.mabroook.Activity.Login_Signup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.MainActivity;
import com.vouchers.mabroook.Activity.Notification_Settings;
import com.vouchers.mabroook.Adapter.SelectCitySpinner;
import com.vouchers.mabroook.Model.CityModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Create_Account_Form extends AppCompatActivity {

    private static TextView dob_tv;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private TextInputEditText first_name, surname, email, phone, passwordET, re_passwordET;
    private TextInputEditText id_number, address_line_1, address_line_2, postcode;

    private Spinner countrySpinner, stateSpinner, citySpinner;

    private ArrayList<CityModel> country_ArrayList = new ArrayList<>();
    private ArrayList<CityModel> state_ArrayList = new ArrayList<>();
    private ArrayList<CityModel> city_ArrayList = new ArrayList<>();

    private UserSession session;

    private String device_token = "";
    private String pre_signup_user_id = "";

    private String googleID = "";

    private RelativeLayout state_layout, city_layout;



    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_form);

        session = new UserSession(Create_Account_Form.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        dob_tv = findViewById(R.id.dob_tv);
        first_name = findViewById(R.id.first_name);
        surname = findViewById(R.id.surname);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        passwordET = findViewById(R.id.passwordET);
        re_passwordET = findViewById(R.id.re_passwordET);
        id_number = findViewById(R.id.id_number);
        address_line_1 = findViewById(R.id.address_line_1);
        address_line_2 = findViewById(R.id.address_line_2);
        postcode = findViewById(R.id.postcode);

        countrySpinner = findViewById(R.id.countrySpinner);
        stateSpinner = findViewById(R.id.stateSpinner);
        citySpinner = findViewById(R.id.citySpinner);

        state_layout = findViewById(R.id.state_layout);
        city_layout = findViewById(R.id.city_layout);

        device_token = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        if (getIntent().getStringExtra("signUp_type").equals("phone")) {
            phone.setText(getIntent().getStringExtra("phone_no"));
            phone.setEnabled(false);
            pre_signup_user_id = getIntent().getStringExtra("pre_signup_user_id");


        } else if (getIntent().getStringExtra("signUp_type").equals("google")){

            String[] words = getIntent().getStringExtra("name").trim().split(" ");

            first_name.setText(words[0]);
            surname.setText(words[1]);
            email.setText(getIntent().getStringExtra("email"));
            googleID = getIntent().getStringExtra("googleID");

            email.setEnabled(false);
        }



        dob_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePickerGraduation");
            }
        });


        findViewById(R.id.signUpBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (first_name.getText().toString().isEmpty()) {
                    Toast.makeText(Create_Account_Form.this, R.string.enter_first_name, Toast.LENGTH_SHORT).show();
                } else if (surname.getText().toString().isEmpty()){
                    Toast.makeText(Create_Account_Form.this, R.string.enter_your_surname, Toast.LENGTH_SHORT).show();
                } else if (email.getText().toString().isEmpty()){
                    Toast.makeText(Create_Account_Form.this, R.string.enter_your_email, Toast.LENGTH_SHORT).show();
                } else if (!email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Create_Account_Form.this, R.string.invalid_email_password,Toast.LENGTH_SHORT).show();
                } else if (dob_tv.getText().toString().isEmpty()){
                    Toast.makeText(Create_Account_Form.this, R.string.enter_dob, Toast.LENGTH_SHORT).show();
                } else if (passwordET.getText().toString().isEmpty()){
                    Toast.makeText(Create_Account_Form.this, R.string.enter_password, Toast.LENGTH_SHORT).show();
                } else if (re_passwordET.getText().toString().isEmpty()){
                    Toast.makeText(Create_Account_Form.this, R.string.enter_re_password, Toast.LENGTH_SHORT).show();
                } else if (!passwordET.getText().toString().equals(re_passwordET.getText().toString())){
                    Toast.makeText(Create_Account_Form.this, R.string.password_does_not_match, Toast.LENGTH_SHORT).show();
                } else {
                    createAccount();
                }


            }
        });


        getCountry();


    }



    private void getCountry() {
        KProgressHUD progressDialog = KProgressHUD.create(Create_Account_Form.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-countries",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        country_ArrayList.clear();
                        progressDialog.dismiss();

                        Log.e("responseCountries", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("countries");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel model = new CityModel();
                                        model.setName(object.getString("country_name"));
                                        model.setId(object.getString("country_id"));
                                        country_ArrayList.add(model);
                                    }

                                    CityModel model = new CityModel();
                                    model.setId("");
                                    model.setName(getString(R.string.select_country));
                                    country_ArrayList.add(model);

                                    SelectCitySpinner adapter1 = new SelectCitySpinner(Create_Account_Form.this, android.R.layout.simple_spinner_item, country_ArrayList);
                                    adapter1.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                    countrySpinner.setAdapter(adapter1);
                                    countrySpinner.setSelection(adapter1.getCount());

                                    countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            if (position == (country_ArrayList.size() - 1)) {
                                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.dark_gray));
                                            } else {
                                                state_layout.setVisibility(View.VISIBLE);
                                                city_layout.setVisibility(View.GONE);

                                                getState(country_ArrayList.get(position).getId());
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Create_Account_Form.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Create_Account_Form.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_Account_Form.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_Account_Form.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_Account_Form.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_Account_Form.this).add(volleyMultipartRequest);
    }

    private void getState(String country_id) {
        KProgressHUD progressDialog = KProgressHUD.create(Create_Account_Form.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-states?country_id=" + country_id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        state_ArrayList.clear();
                        progressDialog.dismiss();

                        Log.e("responseState", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("states");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel model = new CityModel();
                                        model.setName(object.getString("state_name"));
                                        model.setId(object.getString("state_id"));
                                        state_ArrayList.add(model);
                                    }

                                    CityModel model = new CityModel();
                                    model.setId("");
                                    model.setName(getString(R.string.select_state));
                                    state_ArrayList.add(model);

                                    SelectCitySpinner adapter = new SelectCitySpinner(Create_Account_Form.this, android.R.layout.simple_spinner_item, state_ArrayList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                    stateSpinner.setAdapter(adapter);
                                    stateSpinner.setSelection(adapter.getCount());

                                    stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            if (position == (state_ArrayList.size() - 1)) {
                                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.dark_gray));
                                            } else {
                                                city_layout.setVisibility(View.VISIBLE);
                                                getCity(country_id, state_ArrayList.get(position).getId());
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });

                                    if (state_ArrayList.size() < 2){
                                        state_layout.setVisibility(View.GONE);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Create_Account_Form.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Create_Account_Form.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_Account_Form.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_Account_Form.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_Account_Form.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_Account_Form.this).add(volleyMultipartRequest);
    }

    private void getCity(String country_id, String state_id) {
        KProgressHUD progressDialog = KProgressHUD.create(Create_Account_Form.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "get-cities?country_id=" + country_id + "&state_id=" + state_id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        city_ArrayList.clear();
                        progressDialog.dismiss();

                        Log.e("responseCity", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("cities");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel model = new CityModel();
                                        model.setName(object.getString("city_name"));
                                        model.setId(object.getString("city_id"));
                                        city_ArrayList.add(model);
                                    }

                                    CityModel model = new CityModel();
                                    model.setId("");
                                    model.setName(getString(R.string.select_city));
                                    city_ArrayList.add(model);

                                    SelectCitySpinner adapter11 = new SelectCitySpinner(Create_Account_Form.this, android.R.layout.simple_spinner_item, city_ArrayList);
                                    adapter11.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                    citySpinner.setAdapter(adapter11);
                                    citySpinner.setSelection(adapter11.getCount());

                                    citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            if (position == (city_ArrayList.size() - 1)) {
                                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.dark_gray));
                                            } else {

                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Create_Account_Form.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Create_Account_Form.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_Account_Form.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_Account_Form.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_Account_Form.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_Account_Form.this).add(volleyMultipartRequest);
    }



    private void createAccount() {
        KProgressHUD progressDialog = KProgressHUD.create(Create_Account_Form.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "create-account",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        Log.e("create_account", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONObject object = jsonObject.getJSONObject("data");

                                    session.setUSER_ID(object.getString("user_id"));
                                    session.setFIRSTNAME(object.getString("first_name"));
                                    session.setLASTNAME(object.getString("last_name"));
                                    session.setEMAIL(object.getString("email"));
                                    session.setPHONE(object.getString("phone"));
                                    session.setPROFILE_PIC(object.getString("profile_pic"));
                                    session.setFACEBOOK_ID(object.getString("facebook_id"));
                                    session.setGOOGLE_ID(object.getString("google_id"));
                                    session.setLANGUAGE(object.getString("language"));
                                    session.setADDRESS_LINE_1(object.getString("address_line_1"));
                                    session.setADDRESS_LINE_2(object.getString("address_line_2"));
                                    session.setCityId(object.getString("city_id"));
                                    session.setStateId(object.getString("state_id"));
                                    session.setCountryId(object.getString("country_id"));
                                    session.setPostCode(object.getString("postcode"));
                                    session.setLatitude(object.getString("latitude"));
                                    session.setLongitude(object.getString("longitude"));
                                    session.setDOB(object.getString("dob"));
                                    session.setAPITOKEN(object.getString("api_token"));
                                    session.setSTATUS(object.getString("status"));
                                    session.setUSER_ID(object.getString("device_token"));

                                    session.setIsLogin(true);

                                    startActivity(new Intent(Create_Account_Form.this, MainActivity.class));
                                    finish();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Create_Account_Form.this, Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Create_Account_Form.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_Account_Form.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_Account_Form.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_Account_Form.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("first_name", first_name.getText().toString().trim());
                params.put("last_name", surname.getText().toString().trim());
                params.put("email", email.getText().toString().trim());
                params.put("phone", phone.getText().toString());
                params.put("dob", dob_tv.getText().toString());
                params.put("password", passwordET.getText().toString().trim());

                params.put("id_number", id_number.getText().toString());
                params.put("address_line_1", address_line_1.getText().toString());
                params.put("address_line_2", address_line_2.getText().toString());
                params.put("country_id", country_ArrayList.get(countrySpinner.getSelectedItemPosition()).getId());
                if (state_layout.getVisibility() == View.VISIBLE) {
                    params.put("state_id", state_ArrayList.get(stateSpinner.getSelectedItemPosition()).getId());
                }
                if (city_layout.getVisibility() == View.VISIBLE) {
                    params.put("city_id", city_ArrayList.get(citySpinner.getSelectedItemPosition()).getId());
                }
                params.put("postcode", postcode.getText().toString());

                params.put("pre_signup_user_id", pre_signup_user_id);
                params.put("language", "en");
                params.put("google_id", googleID);
                params.put("device_type", "android");
                params.put("device_token", device_token);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_Account_Form.this).add(volleyMultipartRequest);
    }



    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return  dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            dob_tv.setText(dateString);

           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }
    }



}