package com.vouchers.mabroook.Activity.Login_Signup;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Notification_Settings;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Create_account_enter_mobile extends AppCompatActivity {

    private TextView enter_mobile_TV, confirmation_TV, resend_code, timer_resend_code;
    private EditText phone_ET;

    private boolean isOtpSend = false;

    private UserSession session;

    private String phone_no = "";
    private String pre_signup_user_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_enter_mobile);

        session = new UserSession(Create_account_enter_mobile.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        enter_mobile_TV = findViewById(R.id.enter_mobile_TV);
        confirmation_TV = findViewById(R.id.confirmation_TV);
        phone_ET = findViewById(R.id.phone_ET);
        resend_code = findViewById(R.id.resend_code);
        timer_resend_code = findViewById(R.id.timer_resend_code);



        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resend_verify_code();
            }
        });

        findViewById(R.id.nextBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!phone_ET.getText().toString().isEmpty()) {

                    if (isOtpSend) {
                        checkOtp(phone_ET.getText().toString());
                    } else {
                        sendOtp(phone_ET.getText().toString());
                    }
                } else {
                    Toast.makeText(Create_account_enter_mobile.this, R.string.enter_value, Toast.LENGTH_SHORT).show();
                }

            }
        });


    }



    private void sendOtp(String phone_number) {
        final KProgressHUD progressDialog = KProgressHUD.create(Create_account_enter_mobile.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "signup-with-phone",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("checkStatusOtp", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                phone_no = phone_number;

                                enter_mobile_TV.setText(getString(R.string.enter_verification_code));
                                confirmation_TV.setText(getString(R.string.we_have_just_send_a_code_to_number) + phone_number);

                                phone_ET.setText(jsonObject.getJSONObject("data").getString("otp"));
                                pre_signup_user_id = jsonObject.getJSONObject("data").getString("pre_signup_user_id");


                                new CountDownTimer(30000, 1000) {
                                    public void onTick(long millisUntilFinished) {
                                        timer_resend_code.setVisibility(View.VISIBLE);
                                        timer_resend_code.setText(getString(R.string.resend_code_in) + millisUntilFinished / 1000 + " " + getString(R.string.seconds));
                                    }
                                    public void onFinish() {
                                        resend_code.setVisibility(View.VISIBLE);
                                        timer_resend_code.setVisibility(View.GONE);
                                    }
                                }.start();



                                isOtpSend = true;
                            }


                        } catch (Exception e) {

                            Toast.makeText(Create_account_enter_mobile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_account_enter_mobile.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_account_enter_mobile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_account_enter_mobile.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phone_number);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
          //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_account_enter_mobile.this).add(volleyMultipartRequest);
    }


    private void checkOtp(String otp) {
        final KProgressHUD progressDialog = KProgressHUD.create(Create_account_enter_mobile.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "check-verification-code",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("checkStatusOtp", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                startActivity(new Intent(Create_account_enter_mobile.this, Create_Account_Form.class)
                                        .putExtra("signUp_type", "phone")
                                        .putExtra("phone_no", phone_no)
                                        .putExtra("pre_signup_user_id", jsonObject.getJSONObject("data").getString("pre_signup_user_id")));
                                finish();
                            }

                        } catch (Exception e) {

                            Toast.makeText(Create_account_enter_mobile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_account_enter_mobile.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_account_enter_mobile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_account_enter_mobile.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phone_no);
                params.put("otp", otp);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
          //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_account_enter_mobile.this).add(volleyMultipartRequest);
    }


    private void resend_verify_code() {
        KProgressHUD progressDialog = KProgressHUD.create(Create_account_enter_mobile.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "resend-verification-code",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("resend_code_verify", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                phone_ET.setText(jsonObject.getJSONObject("data").getString("otp"));

                                Toast.makeText(Create_account_enter_mobile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                resend_code.setVisibility(View.GONE);
                                new CountDownTimer(30000, 1000) {
                                    public void onTick(long millisUntilFinished) {
                                        timer_resend_code.setVisibility(View.VISIBLE);
                                        timer_resend_code.setText(getString(R.string.resend_code_in) + millisUntilFinished / 1000 + " " + getString(R.string.seconds));
                                    }
                                    public void onFinish() {
                                        timer_resend_code.setVisibility(View.GONE);
                                        resend_code.setVisibility(View.VISIBLE);
                                    }
                                }.start();

                            }

                        } catch (Exception e) {
                            Toast.makeText(Create_account_enter_mobile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Create_account_enter_mobile.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Create_account_enter_mobile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Create_account_enter_mobile.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("pre_signup_user_id", pre_signup_user_id);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Create_account_enter_mobile.this).add(volleyMultipartRequest);
    }


}