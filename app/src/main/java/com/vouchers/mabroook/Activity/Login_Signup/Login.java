package com.vouchers.mabroook.Activity.Login_Signup;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.MainActivity;
import com.vouchers.mabroook.Activity.Notification_Settings;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private EditText passwordET, email;
    private Boolean isPasswordVisible = false;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private UserSession session;

    private String android_id = "";


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new UserSession(Login.this);


        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        passwordET = findViewById(R.id.passwordET);
        email = findViewById(R.id.email);


        findViewById(R.id.forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this, ResetPassword1.class));
            }
        });

        findViewById(R.id.loginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (email.getText().toString().isEmpty()){
                    Toast.makeText(Login.this, R.string.enter_your_email, Toast.LENGTH_SHORT).show();
                } else if (!email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Login.this,R.string.invalid_email_password,Toast.LENGTH_SHORT).show();
                } else if (passwordET.getText().toString().isEmpty()){
                    Toast.makeText(Login.this, R.string.enter_password, Toast.LENGTH_SHORT).show();
                } else {
                    logIn();
                }

            }
        });



        passwordET.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = false;
                final int RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (passwordET.getRight() - passwordET.getCompoundDrawables()[RIGHT].getBounds().width())) {
                        int selection = passwordET.getSelectionEnd();
                        if (isPasswordVisible) {
                            // set drawable image
                            passwordET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0);
                            // hide Password
                            passwordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            isPasswordVisible = false;
                        } else {
                            // set drawable image
                            passwordET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_black_24dp, 0);
                            // show Password
                            passwordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            isPasswordVisible = true;
                        }
                        passwordET.setSelection(selection);
                        result = true;
                    }
                }
                return result;
            }
        });



    }


    private void logIn() {
        KProgressHUD progressDialog = KProgressHUD.create(Login.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "login",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("loginStatus", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                session.setUSER_ID(object.getString("user_id"));
                                session.setFIRSTNAME(object.getString("first_name"));
                                session.setLASTNAME(object.getString("last_name"));
                                session.setEMAIL(object.getString("email"));
                                session.setPHONE(object.getString("phone"));
                                session.setPROFILE_PIC(object.getString("profile_pic"));
                                session.setFACEBOOK_ID(object.getString("facebook_id"));
                                session.setGOOGLE_ID(object.getString("google_id"));
                                session.setLANGUAGE(object.getString("language"));
                                session.setADDRESS_LINE_1(object.getString("address_line_1"));
                                session.setADDRESS_LINE_2(object.getString("address_line_2"));
                                session.setCityId(object.getString("city_id"));
                                session.setStateId(object.getString("state_id"));
                                session.setCountryId(object.getString("country_id"));
                                session.setPostCode(object.getString("postcode"));
                                session.setLatitude(object.getString("latitude"));
                                session.setLongitude(object.getString("longitude"));
                                session.setDOB(object.getString("dob"));
                                session.setAPITOKEN(object.getString("api_token"));
                                session.setSTATUS(object.getString("status"));
                                session.setUSER_ID(object.getString("device_token"));

                                session.setIsLogin(true);

                                startActivity(new Intent(Login.this, MainActivity.class));
                                finish();
                            }

                        } catch (Exception e) {

                            Toast.makeText(Login.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Login.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Login.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Login.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email.getText().toString().trim());
                params.put("password", passwordET.getText().toString());
                params.put("device_type", "android");
                params.put("device_token", android_id);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Login.this).add(volleyMultipartRequest);
    }



}