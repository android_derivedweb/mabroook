package com.vouchers.mabroook.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Change_password extends AppCompatActivity {

    private UserSession session;
    private Boolean isPasswordVisible = false, isPasswordVisible1 = false, isPasswordVisible2 = false;
    private EditText old_pass_ET, new_pass_ET, confirm_pass_ET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        session = new UserSession(Change_password.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        old_pass_ET = findViewById(R.id.old_pass_ET);
        new_pass_ET = findViewById(R.id.new_pass_ET);
        confirm_pass_ET = findViewById(R.id.confirm_pass_ET);


        old_pass_ET.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = false;
                final int RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (old_pass_ET.getRight() - old_pass_ET.getCompoundDrawables()[RIGHT].getBounds().width())) {
                        int selection = old_pass_ET.getSelectionEnd();
                        if (isPasswordVisible) {
                            // set drawable image
                            old_pass_ET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0);
                            // hide Password
                            old_pass_ET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            isPasswordVisible = false;
                        } else {
                            // set drawable image
                            old_pass_ET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_black_24dp, 0);
                            // show Password
                            old_pass_ET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            isPasswordVisible = true;
                        }
                        old_pass_ET.setSelection(selection);
                        result = true;
                    }
                }
                return result;
            }
        });


        new_pass_ET.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = false;
                final int RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (new_pass_ET.getRight() - new_pass_ET.getCompoundDrawables()[RIGHT].getBounds().width())) {
                        int selection = new_pass_ET.getSelectionEnd();
                        if (isPasswordVisible1) {
                            // set drawable image
                            new_pass_ET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0);
                            // hide Password
                            new_pass_ET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            isPasswordVisible1 = false;
                        } else {
                            // set drawable image
                            new_pass_ET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_black_24dp, 0);
                            // show Password
                            new_pass_ET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            isPasswordVisible1 = true;
                        }
                        new_pass_ET.setSelection(selection);
                        result = true;
                    }
                }
                return result;
            }
        });


        confirm_pass_ET.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = false;
                final int RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (confirm_pass_ET.getRight() - confirm_pass_ET.getCompoundDrawables()[RIGHT].getBounds().width())) {
                        int selection = confirm_pass_ET.getSelectionEnd();
                        if (isPasswordVisible2) {
                            // set drawable image
                            confirm_pass_ET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0);
                            // hide Password
                            confirm_pass_ET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            isPasswordVisible2 = false;
                        } else {
                            // set drawable image
                            confirm_pass_ET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_black_24dp, 0);
                            // show Password
                            confirm_pass_ET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            isPasswordVisible2 = true;
                        }
                        confirm_pass_ET.setSelection(selection);
                        result = true;
                    }
                }
                return result;
            }
        });


        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (old_pass_ET.getText().toString().isEmpty()){
                    Toast.makeText(Change_password.this, R.string.enter_old_password, Toast.LENGTH_SHORT).show();
                } else if (new_pass_ET.getText().toString().isEmpty()){
                    Toast.makeText(Change_password.this, R.string.enter_new_password, Toast.LENGTH_SHORT).show();
                } else if (confirm_pass_ET.getText().toString().isEmpty()){
                    Toast.makeText(Change_password.this, R.string.enter_confirm_password, Toast.LENGTH_SHORT).show();
                } else if (!new_pass_ET.getText().toString().equals(confirm_pass_ET.getText().toString())){
                    Toast.makeText(Change_password.this, R.string.password_does_not_match, Toast.LENGTH_SHORT).show();
                }else {
                    matchOldPassword();
                }
            }
        });


    }


    private void matchOldPassword() {
        final KProgressHUD progressDialog = KProgressHUD.create(Change_password.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "match-old-password",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("responceDeactivate", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                updatePassword();

                            } else if (jsonObject.getString("ResponseCode").equals("401")) {

                                Toast.makeText(Change_password.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Change_password.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        /*session.logout();
                        Intent intent = new Intent(getActivity(), Activity_SelectCity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();*/

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_password.this, json, Toast.LENGTH_LONG).show();
                                            Log.e("error_update_profile", json + "");
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_password.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_password.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("old_password", old_pass_ET.getText().toString());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(Change_password.this).add(volleyMultipartRequest);

    }


    private void updatePassword() {
        final KProgressHUD progressDialog = KProgressHUD.create(Change_password.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
          //      .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "update-change-password",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                //        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("responceDeactivate", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                Toast.makeText(Change_password.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            } else if (jsonObject.getString("ResponseCode").equals("401")) {

                                Toast.makeText(Change_password.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Change_password.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
           //             progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Change_password.this, json, Toast.LENGTH_LONG).show();
                                            Log.e("error_update_profile", json + "");
                                        }
                                        break;
                                }
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Change_password.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Change_password.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){

                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", new_pass_ET.getText().toString());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(Change_password.this).add(volleyMultipartRequest);

    }


}