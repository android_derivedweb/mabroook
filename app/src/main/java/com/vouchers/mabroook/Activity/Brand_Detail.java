package com.vouchers.mabroook.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Brand_Detail extends AppCompatActivity {

    private TextView description_TV, read_more_tv;
    private boolean expand;
    private boolean expandable;

    private ImageView brand_icon;
    private TextView tag_line_TV, brand_name_TV;
    private RelativeLayout background_RL;

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_detail);

        session = new UserSession(Brand_Detail.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        description_TV = findViewById(R.id.description_TV);
        read_more_tv = findViewById(R.id.read_more_tv);

        brand_icon = findViewById(R.id.brand_icon);
        tag_line_TV = findViewById(R.id.tag_line_TV);
        brand_name_TV = findViewById(R.id.brand_name_TV);
        background_RL = findViewById(R.id.background_RL);


        String brand_ID = getIntent().getStringExtra("brand_id");


        description_TV.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(expandable) {
                    expandable = false;
                    if (description_TV.getLineCount() > 3) {
                        read_more_tv.setVisibility(View.VISIBLE);
                        ObjectAnimator animation = ObjectAnimator.ofInt(description_TV, "maxLines", 3);
                        animation.setDuration(0).start();
                    }
                }
            }
        });


        read_more_tv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!expand) {
                    expand = true;
                    ObjectAnimator animation = ObjectAnimator.ofInt(description_TV, "maxLines", 40);
                    animation.setDuration(100).start();

                    read_more_tv.setVisibility(View.GONE);

                } else {
                    expand = false;
                    ObjectAnimator animation = ObjectAnimator.ofInt(description_TV, "maxLines", 3);
                    animation.setDuration(100).start();
                }

            }
        });


        getBrandCat(brand_ID);

    }


    private void getBrandCat(String brand_ID) {
        KProgressHUD progressDialog = KProgressHUD.create(Brand_Detail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-brand-details?brand_id=" + brand_ID,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("brand_Detail", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                description_TV.setText(object.getString("description"));
                                Glide.with(Brand_Detail.this).load(object.getString("brand_logo")).into(brand_icon);

                                brand_name_TV.setText(object.getString("brand_name"));
                                tag_line_TV.setText(object.getString("tag_line"));

                                setImagebackground(background_RL, object.getString("background_image"));

                            }

                        } catch (Exception e) {
                            Toast.makeText(Brand_Detail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(Brand_Detail.this, json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Brand_Detail.this, Login_SignUp.class));
                                finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(Brand_Detail.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Brand_Detail.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Brand_Detail.this).add(volleyMultipartRequest);
    }

    public static void setImagebackground(View view,String url) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL x2 = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) x2.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Drawable dr = new BitmapDrawable(myBitmap);
            view.setBackgroundDrawable(dr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}