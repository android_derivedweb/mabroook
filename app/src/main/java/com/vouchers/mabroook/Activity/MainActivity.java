package com.vouchers.mabroook.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.vouchers.mabroook.Fragment.Main_Bag_Frag;
import com.vouchers.mabroook.Fragment.Main_Home_Frag;
import com.vouchers.mabroook.Fragment.Main_Trophy_Frag;
import com.vouchers.mabroook.R;

public class MainActivity extends AppCompatActivity {

    private ImageView homeIcon, trophyIcon, bagIcon;
    private LinearLayout homeLayout, trophyLayout, bagLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        homeIcon = findViewById(R.id.homeIcon);
        trophyIcon = findViewById(R.id.trophyIcon);
        bagIcon = findViewById(R.id.bagIcon);
        homeLayout = findViewById(R.id.homeLayout);
        trophyLayout = findViewById(R.id.trophyLayout);
        bagLayout = findViewById(R.id.bagLayout);



        Main_Home_Frag home_frag = new Main_Home_Frag();
        replaceFragmentFirst(R.id.fragmentLinearHome, home_frag, "Home_Frag");
        homeIcon.setImageResource(R.drawable.home_fill);
        trophyIcon.setImageResource(R.drawable.trophy);
        bagIcon.setImageResource(R.drawable.bag);


        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Main_Home_Frag home_frag = new Main_Home_Frag();
                replaceFragment(R.id.fragmentLinearHome, home_frag, "Home_Frag");

                homeIcon.setImageResource(R.drawable.home_fill);
                trophyIcon.setImageResource(R.drawable.trophy);
                bagIcon.setImageResource(R.drawable.bag);
            }
        });

        trophyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Main_Trophy_Frag trophy_frag = new Main_Trophy_Frag();
                replaceFragment(R.id.fragmentLinearHome, trophy_frag, "Trophy_Frag");

                homeIcon.setImageResource(R.drawable.home);
                trophyIcon.setImageResource(R.drawable.trophy_fill);
                bagIcon.setImageResource(R.drawable.bag);
            }
        });

        bagLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Main_Bag_Frag bag_frag = new Main_Bag_Frag();
                replaceFragment(R.id.fragmentLinearHome, bag_frag, "Bag_Frag");

                homeIcon.setImageResource(R.drawable.home);
                trophyIcon.setImageResource(R.drawable.trophy);
                bagIcon.setImageResource(R.drawable.bag_fill);
            }
        });


    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit();
    }
    protected void replaceFragmentFirst(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .commit();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        homeIcon.setImageResource(R.drawable.home_fill);
        trophyIcon.setImageResource(R.drawable.trophy);
        bagIcon.setImageResource(R.drawable.bag);
    }



    @Override
    protected void onStart() {
        super.onStart();

        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
      /*  GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        Log.e("checkGoogleLogin", account + "--");*/


       /* // facebook existing user check
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();*/



    }


}
