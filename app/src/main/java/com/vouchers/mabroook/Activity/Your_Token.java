package com.vouchers.mabroook.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vouchers.mabroook.Fragment.Active_Frag;
import com.vouchers.mabroook.Fragment.Expired_Frag;
import com.vouchers.mabroook.Fragment.Inactive_Frag;
import com.vouchers.mabroook.Fragment.Main_Bag_Frag;
import com.vouchers.mabroook.R;

public class Your_Token extends AppCompatActivity {

    public static TextView activeTV, expiredTV, inactiveTV;
    public static LinearLayout inactiveLayout;

    public static TextView inactive_count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_token);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        activeTV = findViewById(R.id.activeTV);
        inactiveTV = findViewById(R.id.inactiveTV);
        expiredTV = findViewById(R.id.expiredTV);
        inactiveLayout = findViewById(R.id.inactiveLayout);
        inactive_count = findViewById(R.id.inactive_count);


        Active_Frag active_frag = new Active_Frag();
        replaceFragment(R.id.fragmentLinear, active_frag, "Active_Frag");

        activeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Active_Frag active_frag = new Active_Frag();
                replaceFragment(R.id.fragmentLinear, active_frag, "Active_Frag");

                activeTV.setTextColor(getResources().getColor(R.color.white));
                inactiveTV.setTextColor(getResources().getColor(R.color.black1));
                expiredTV.setTextColor(getResources().getColor(R.color.black1));

                activeTV.setBackgroundResource(R.drawable.round_orange_8dp);
                inactiveLayout.setBackgroundResource(R.drawable.round_white_8dp);
                expiredTV.setBackgroundResource(R.drawable.round_white_8dp);
            }
        });

        inactiveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Inactive_Frag inactive_frag = new Inactive_Frag();
                replaceFragment(R.id.fragmentLinear, inactive_frag, "Inactive_Frag");

                activeTV.setTextColor(getResources().getColor(R.color.black1));
                inactiveTV.setTextColor(getResources().getColor(R.color.white));
                expiredTV.setTextColor(getResources().getColor(R.color.black1));

                activeTV.setBackgroundResource(R.drawable.round_white_8dp);
                inactiveLayout.setBackgroundResource(R.drawable.round_orange_8dp);
                expiredTV.setBackgroundResource(R.drawable.round_white_8dp);
            }
        });

        expiredTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Expired_Frag expired_frag = new Expired_Frag();
                replaceFragment(R.id.fragmentLinear, expired_frag, "Expired_Frag");

                activeTV.setTextColor(getResources().getColor(R.color.black1));
                inactiveTV.setTextColor(getResources().getColor(R.color.black1));
                expiredTV.setTextColor(getResources().getColor(R.color.white));

                activeTV.setBackgroundResource(R.drawable.round_white_8dp);
                inactiveLayout.setBackgroundResource(R.drawable.round_white_8dp);
                expiredTV.setBackgroundResource(R.drawable.round_orange_8dp);
            }
        });


    }



    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


}