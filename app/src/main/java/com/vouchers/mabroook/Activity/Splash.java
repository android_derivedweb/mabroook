package com.vouchers.mabroook.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;

public class Splash extends AppCompatActivity {

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        session = new UserSession(Splash.this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (session.isLoggedIn()){
                    startActivity(new Intent(Splash.this, MainActivity.class));
                } else {
                    startActivity(new Intent(Splash.this, Login_SignUp.class));
                }
                finish();

            }
        }, 500);


    }


}