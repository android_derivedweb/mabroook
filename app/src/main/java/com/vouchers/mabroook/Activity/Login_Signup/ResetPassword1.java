package com.vouchers.mabroook.Activity.Login_Signup;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.MainActivity;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPassword1 extends AppCompatActivity {

    private UserSession session;
    private TextInputEditText email_ET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password1);

        session = new UserSession(ResetPassword1.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        email_ET = findViewById(R.id.email_ET);



        findViewById(R.id.getCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (email_ET.getText().toString().isEmpty()){
                    Toast.makeText(ResetPassword1.this, R.string.enter_phone_email, Toast.LENGTH_SHORT).show();
                } else {
                    codeSent();
                }
            }
        });

    }



    private void codeSent() {
        KProgressHUD progressDialog = KProgressHUD.create(ResetPassword1.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "forgot-password",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("check_email_phone", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                startActivity(new Intent(ResetPassword1.this, ResetPassword2.class)
                                        .putExtra("is_email", object.getString("is_email"))
                                        .putExtra("user_id", object.getString("user_id"))
                                        .putExtra("otp", object.getString("otp")));

                                finish();

                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                openPopUp();
                            }

                        } catch (Exception e) {
                            Toast.makeText(ResetPassword1.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(ResetPassword1.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ResetPassword1.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ResetPassword1.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(ResetPassword1.this).add(volleyMultipartRequest);
    }




    private void openPopUp() {
        Dialog dialog1 = new Dialog(ResetPassword1.this);
        dialog1.setContentView(R.layout.dialog_try_again);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog1.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog1.setCancelable(true);


        dialog1.findViewById(R.id.try_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              dialog1.dismiss();
            }
        });

        dialog1.show();
    }

}