package com.vouchers.mabroook.Activity;

import static com.bumptech.glide.util.Preconditions.checkArgument;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Adapter.Adapter_winners;
import com.vouchers.mabroook.Model.RaffleModel;
import com.vouchers.mabroook.Model.WinnersModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Trophy_Winners_Inner extends AppCompatActivity {

    private Adapter_winners adapter_winners;
    private RecyclerView recWinners;

    private TextView first_name, second_name, third_name;
    private TextView draw_uid, dateTV;

    private UserSession session;

    private ArrayList<WinnersModel> winnersModelArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trophy_winners_inner);

        session = new UserSession(this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        first_name = findViewById(R.id.first_name);
        second_name = findViewById(R.id.second_name);
        third_name = findViewById(R.id.third_name);
        draw_uid = findViewById(R.id.draw_uid);
        dateTV = findViewById(R.id.dateTV);


        String draw_id = getIntent().getStringExtra("draw_id");


        recWinners = findViewById(R.id.recWinners);
        recWinners.setLayoutManager(new LinearLayoutManager(this));
        adapter_winners = new Adapter_winners(this, winnersModelArrayList, new Adapter_winners.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recWinners.setAdapter(adapter_winners);


        winnersDetail(draw_id);


    }


    private void winnersDetail(String draw_id) {
        KProgressHUD progressDialog = KProgressHUD.create(Trophy_Winners_Inner.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "our-winner-details?draw_id=" + draw_id,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("listDetailResponse", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                draw_uid.setText(object.getString("draw_uid"));


                                JSONArray array = object.getJSONArray("winners");

                                first_name.setText(array.getJSONObject(0).getString("first_name"));
                                second_name.setText(array.getJSONObject(1).getString("first_name"));
                                third_name.setText(array.getJSONObject(2).getString("first_name"));

                                for (int i = 3; i < array.length(); i++){
                                    JSONObject object1 = array.getJSONObject(i);

                                    WinnersModel model1 = new WinnersModel();
                                    model1.setRaffle_uid(object1.getString("raffle_uid"));
                                    model1.setPrize_title(object1.getString("prize_title"));
                                    model1.setFirst_name(object1.getString("first_name"));
                                    model1.setPrize_number(object1.getString("prize_number"));

                                    winnersModelArrayList.add(model1);
                                }

                                adapter_winners.notifyDataSetChanged();


                                DateFormat inputFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                                DateFormat outputFormat = new SimpleDateFormat("MMM yyyy");
                                DateFormat outputFormat1 = new SimpleDateFormat("dd");

                                String inputDateStr = object.getString("draw_datetime");

                                Date date = null;
                                try {
                                    date = inputFormat.parse(inputDateStr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String outputDateStr = outputFormat.format(date);
                                String dateStr = getDayOfMonthSuffix(Integer.parseInt(outputFormat1.format(date)));

                                dateTV.setText(outputFormat1.format(date) + "" + dateStr + " " + outputDateStr);


                            }


                        } catch (Exception e) {
                            Toast.makeText(Trophy_Winners_Inner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(Trophy_Winners_Inner.this, json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Trophy_Winners_Inner.this, Login_SignUp.class));
                                finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(Trophy_Winners_Inner.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Trophy_Winners_Inner.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Trophy_Winners_Inner.this).add(volleyMultipartRequest);
    }


    String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }


}