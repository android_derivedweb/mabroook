package com.vouchers.mabroook.Activity.Login_Signup;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Notification_Settings;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPassword2 extends AppCompatActivity {

    private TextView resend_code, timer_resend_code, instruction_TV;
    private ImageView imageIcon;
    private TextInputEditText code_ET;

    private UserSession session;

    private String is_email = "";
    private String user_id = "";
    private String otp = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password2_sms);


        session = new UserSession(ResetPassword2.this);


        resend_code = findViewById(R.id.resend_code);
        timer_resend_code = findViewById(R.id.timer_resend_code);
        imageIcon = findViewById(R.id.imageIcon);
        instruction_TV = findViewById(R.id.instruction_TV);
        code_ET = findViewById(R.id.code_ET);


        imageIcon.setImageResource(R.drawable.message_icon);
        instruction_TV.setText(getResources().getString(R.string.please_enter_the_x_digit_code_you_will_receive_this_in_your_email));


        is_email = getIntent().getStringExtra("is_email");
        user_id = getIntent().getStringExtra("user_id");

        code_ET.setText(getIntent().getStringExtra("otp"));



        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        findViewById(R.id.getCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (code_ET.getText().toString().isEmpty()){
                    Toast.makeText(ResetPassword2.this, R.string.enter_code, Toast.LENGTH_SHORT).show();
                } else {
                    verify_code();
                }

            }
        });



        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resend_verify_code();

            }
        });


    }


    private void verify_code() {
        KProgressHUD progressDialog = KProgressHUD.create(ResetPassword2.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "check-reset-verification-code",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("code_verify", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                startActivity(new Intent(ResetPassword2.this, ResetPassword3.class)
                                        .putExtra("user_id", user_id));
                                finish();

                            }

                        } catch (Exception e) {
                            Toast.makeText(ResetPassword2.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(ResetPassword2.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ResetPassword2.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ResetPassword2.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("otp", code_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(ResetPassword2.this).add(volleyMultipartRequest);
    }


    private void resend_verify_code() {
        KProgressHUD progressDialog = KProgressHUD.create(ResetPassword2.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "resend-reset-verification-code",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("resend_code_verify", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                Toast.makeText(ResetPassword2.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                findViewById(R.id.do_not_get_sms).setVisibility(View.INVISIBLE);
                                resend_code.setVisibility(View.GONE);
                                new CountDownTimer(30000, 1000) {
                                    public void onTick(long millisUntilFinished) {
                                        timer_resend_code.setVisibility(View.VISIBLE);
                                        timer_resend_code.setText(getString(R.string.resend_code_in) + millisUntilFinished / 1000 + " " + getString(R.string.seconds));
                                    }
                                    public void onFinish() {
                                        timer_resend_code.setVisibility(View.INVISIBLE);
                                        resend_code.setVisibility(View.VISIBLE);
                                    }
                                }.start();

                            }

                        } catch (Exception e) {
                            Toast.makeText(ResetPassword2.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(ResetPassword2.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ResetPassword2.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ResetPassword2.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("is_email", is_email);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(ResetPassword2.this).add(volleyMultipartRequest);
    }



}