package com.vouchers.mabroook.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Adapter.Adapter_search;
import com.vouchers.mabroook.Model.BrandModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Search extends AppCompatActivity {

    private RecyclerView recRecommended, recSearch;
    private Adapter_search adapter_recommended, adapter_search;
    private EditText search_ET;

    private UserSession session;

    private ArrayList<BrandModel> recommendedArrayList = new ArrayList<>();
    private ArrayList<BrandModel> searchArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        session = new UserSession(Search.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        search_ET = findViewById(R.id.search_ET);


        recRecommended = findViewById(R.id.recRecommended);
        recRecommended.setLayoutManager(new LinearLayoutManager(Search.this));
        adapter_recommended = new Adapter_search(Search.this, recommendedArrayList, new Adapter_search.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(Search.this, Brand_Detail.class)
                        .putExtra("brand_id", recommendedArrayList.get(item).getBrand_id()));
            }
        });
        recRecommended.setAdapter(adapter_recommended);

        recSearch = findViewById(R.id.recSearch);
        recSearch.setLayoutManager(new LinearLayoutManager(Search.this));
        adapter_search = new Adapter_search(Search.this, searchArrayList, new Adapter_search.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(Search.this, Brand_Detail.class)
                        .putExtra("brand_id", searchArrayList.get(item).getBrand_id()));
            }
        });
        recSearch.setAdapter(adapter_search);


        findViewById(R.id.searchIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSearch(search_ET.getText().toString().trim());
            }
        });

        search_ET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getSearch(search_ET.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });


        getSearch("");


    }



    private void getSearch(String brand_name) {
        KProgressHUD progressDialog = KProgressHUD.create(Search.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "search-brands?brand_name=" + brand_name,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        recommendedArrayList.clear();
                        searchArrayList.clear();

                        try {
                            Log.e("search_data", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                JSONArray array = object.getJSONArray("recommended_brands");
                                for (int i = 0; i < array.length(); i++){
                                    JSONObject object1 = array.getJSONObject(i);

                                    BrandModel model = new BrandModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setBrand_logo(object1.getString("brand_icon"));
                                    model.setBrand_name(object1.getString("brand_name"));

                                    recommendedArrayList.add(model);
                                }

                                JSONArray array1 = object.getJSONArray("searched_brands");
                                for (int i = 0; i < array1.length(); i++){
                                    JSONObject object1 = array1.getJSONObject(i);

                                    BrandModel model = new BrandModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setBrand_logo(object1.getString("brand_icon"));
                                    model.setBrand_name(object1.getString("brand_name"));

                                    searchArrayList.add(model);
                                }

                                adapter_recommended.notifyDataSetChanged();
                                adapter_search.notifyDataSetChanged();

                            }

                        } catch (Exception e) {
                            Toast.makeText(Search.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(Search.this, json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Search.this, Login_SignUp.class));
                                finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(Search.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Search.this, R.string.bad_networ_connection, Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Search.this).add(volleyMultipartRequest);
    }



}