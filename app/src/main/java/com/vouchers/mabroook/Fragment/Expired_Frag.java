package com.vouchers.mabroook.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Adapter.Adapter_expired;
import com.vouchers.mabroook.Model.ActivateModel;
import com.vouchers.mabroook.Model.RecentlyActivateModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Expired_Frag extends Fragment {

    private RecyclerView recExpired;
    private Adapter_expired adapter_expired;

    private UserSession session;
    private ArrayList<RecentlyActivateModel> expiredArrayList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expired, container, false);

        session = new UserSession(getContext());

        recExpired = view.findViewById(R.id.recExpired);
        recExpired.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_expired = new Adapter_expired(getContext(), expiredArrayList, new Adapter_expired.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recExpired.setAdapter(adapter_expired);


        view.findViewById(R.id.clearVouchers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear_vouchers();
            }
        });


        getExpiredTokens();

        return view;
    }


    private void getExpiredTokens() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "my-vouchers?status=expired",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        expiredArrayList.clear();

                        try {
                            Log.e("expired_response", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                JSONArray array3 = object.getJSONArray("vouchers");
                                for (int i = 0; i < array3.length(); i++){
                                    JSONObject object1 = array3.getJSONObject(i);

                                    RecentlyActivateModel model = new RecentlyActivateModel();
                                    model.setVoucher_id(object1.getString("voucher_id"));
                                    model.setVoucher_uid(object1.getString("voucher_uid"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setVoucher_status(object1.getString("voucher_status"));

                                    expiredArrayList.add(model);
                                }

                                adapter_expired.notifyDataSetChanged();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


    private void clear_vouchers() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "clear-vouchers",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("clear_vouchers", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                expiredArrayList.clear();
                                adapter_expired.notifyDataSetChanged();

                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


}