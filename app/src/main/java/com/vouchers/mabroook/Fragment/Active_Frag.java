package com.vouchers.mabroook.Fragment;

import static com.vouchers.mabroook.Activity.Your_Token.activeTV;
import static com.vouchers.mabroook.Activity.Your_Token.expiredTV;
import static com.vouchers.mabroook.Activity.Your_Token.inactiveLayout;
import static com.vouchers.mabroook.Activity.Your_Token.inactiveTV;
import static com.vouchers.mabroook.Activity.Your_Token.inactive_count;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Adapter.Adapter_brand;
import com.vouchers.mabroook.Model.CityModel;
import com.vouchers.mabroook.Model.RecentlyActivateModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Active_Frag extends Fragment {

    private RecyclerView recBrand;
    private Adapter_brand adapter_brand;

    private UserSession session;

    private ArrayList<CityModel> brandArrayList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active, container, false);

        session = new UserSession(getContext());


        view.findViewById(R.id.activeInactivateTokens).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inactive_Frag inactive_frag = new Inactive_Frag();
                replaceFragment(R.id.fragmentLinear, inactive_frag, "Inactive_Frag");

                activeTV.setTextColor(getResources().getColor(R.color.black1));
                inactiveTV.setTextColor(getResources().getColor(R.color.white));
                expiredTV.setTextColor(getResources().getColor(R.color.black1));

                activeTV.setBackgroundResource(R.drawable.round_white_8dp);
                inactiveLayout.setBackgroundResource(R.drawable.round_orange_8dp);
                expiredTV.setBackgroundResource(R.drawable.round_white_8dp);
            }
        });




        recBrand = view.findViewById(R.id.recBrand);
        recBrand.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_brand = new Adapter_brand(getContext(), brandArrayList, new Adapter_brand.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recBrand.setAdapter(adapter_brand);


        getActiveTokens();

        return view;
    }



    private void getActiveTokens() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "my-vouchers?status=active",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            Log.e("active_response", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                inactive_count.setText(object.getString("total_inactive_tokens"));

                                JSONArray array3 = object.getJSONArray("vouchers");
                                for (int i = 0; i < array3.length(); i++){
                                    JSONObject object1 = array3.getJSONObject(i);

                                    CityModel model1 = new CityModel();
                                    model1.setId(object1.getString("partner_id"));
                                    model1.setName(object1.getString("brand_name"));

                                    ArrayList<RecentlyActivateModel> recentlyActivateArrayList = new ArrayList<>();

                                    JSONArray array = object1.getJSONArray("vouchers");

                                    for (int j = 0; j < array.length(); j++){
                                        JSONObject object2 = array.getJSONObject(j);

                                        RecentlyActivateModel model = new RecentlyActivateModel();
                                        model.setVoucher_id(object2.getString("voucher_id"));
                                        model.setRaffle_id(object2.getString("raffle_id"));
                                        model.setVoucher_uid(object2.getString("voucher_uid"));
                                        model.setRaffle_uid(object2.getString("raffle_uid"));
                                        model.setBrand_logo(object2.getString("brand_logo"));
                                        model.setRaffle_status(object2.getString("raffle_status"));
                                        model.setActivation_date(object2.getString("activation_date"));
                                        model.setMultiple_draw_name(object2.getString("multiple_draw_name"));
                                        model.setRaffle_cnt(object2.getString("raffle_cnt"));
                                        model.setDraw_datetime(object2.getString("draw_datetime"));
                                        model.setVoucher_status(object2.getString("voucher_status"));

                                        recentlyActivateArrayList.add(model);
                                    }
                                    model1.setRecentlyActivateArrayList(recentlyActivateArrayList);

                                    brandArrayList.add(model1);
                                }

                                adapter_brand.notifyDataSetChanged();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }



    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


}