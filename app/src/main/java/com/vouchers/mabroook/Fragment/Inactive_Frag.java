package com.vouchers.mabroook.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Activate_Tokens;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Adapter.Adapter_active_tokens;
import com.vouchers.mabroook.Model.ActivateModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Inactive_Frag extends Fragment {

    private RecyclerView rec_Active_Tokens;
    private Adapter_active_tokens adapter_active_tokens;

    private ArrayList<ActivateModel> activateArrayList = new ArrayList<>();

    private UserSession session;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inactive, container, false);

        session = new UserSession(getContext());

        rec_Active_Tokens = view.findViewById(R.id.rec_Active_Tokens);


        view.findViewById(R.id.activeAllTokens).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPopUp();
            }
        });


        rec_Active_Tokens.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_active_tokens = new Adapter_active_tokens(getContext(), activateArrayList, new Adapter_active_tokens.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), Activate_Tokens.class)
                        .putExtra("partner_id", activateArrayList.get(item).getPartner_id())
                        .putExtra("voucher_cnt", activateArrayList.get(item).getVoucher_cnt()));
            }
        });
        rec_Active_Tokens.setAdapter(adapter_active_tokens);


        getInactiveDetail();

        return view;
    }



    private void getInactiveDetail() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "my-vouchers?status=inactive",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        activateArrayList.clear();

                        try {
                            Log.e("inactive_vouchers", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                JSONArray array3 = object.getJSONArray("partners");
                                for (int i = 0; i < array3.length(); i++){
                                    JSONObject object1 = array3.getJSONObject(i);

                                    ActivateModel model = new ActivateModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setPartner_name(object1.getString("partner_name"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setBrand_name(object1.getString("brand_name"));
                                    model.setBackground_color(object1.getString("background_color"));
                                    model.setVoucher_cnt(object1.getString("voucher_cnt"));
                                    model.setPartner_id(object1.getString("partner_id"));
                                    activateArrayList.add(model);
                                }

                                adapter_active_tokens.notifyDataSetChanged();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }



    private void openPopUp() {

        Dialog dialog1 = new Dialog(getContext());
        dialog1.setContentView(R.layout.dailog_activate_all_info);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog1.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog1.setCancelable(true);


        dialog1.findViewById(R.id.activate_all_vouchers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                activate_all_tokens();
            }
        });

        dialog1.findViewById(R.id.closeImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        dialog1.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

        dialog1.show();
    }


    private void activate_all_tokens() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "activate-all-tokens",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("activate_all_tokens", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }

}