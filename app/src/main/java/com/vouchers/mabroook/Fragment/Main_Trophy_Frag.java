package com.vouchers.mabroook.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Activity.Login_Signup.ResetPassword1;
import com.vouchers.mabroook.Activity.Login_Signup.ResetPassword2;
import com.vouchers.mabroook.Activity.Trophy_Winners_Inner;
import com.vouchers.mabroook.Adapter.Adapter_our_winners;
import com.vouchers.mabroook.Model.RaffleModel;
import com.vouchers.mabroook.Model.WinnersModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Main_Trophy_Frag extends Fragment {

    private Adapter_our_winners adapter_our_winners;
    private RecyclerView recOurWinners;

    private UserSession session;

    private ArrayList<RaffleModel> raffleModelArrayList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_trophy, container, false);

        session = new UserSession(getActivity());


        recOurWinners = view.findViewById(R.id.recOurWinners);
        recOurWinners.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_our_winners = new Adapter_our_winners(getContext(), raffleModelArrayList, new Adapter_our_winners.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), Trophy_Winners_Inner.class)
                        .putExtra("draw_id", raffleModelArrayList.get(item).getDraw_id()));
            }
        });
        recOurWinners.setAdapter(adapter_our_winners);



        trophyList();

        return view;
    }



    private void trophyList() {
        KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "our-winners",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("listTrophyResponse", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray array = jsonObject.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++){
                                    JSONObject object = array.getJSONObject(i);
                                    JSONObject object1 = object.getJSONObject("raffle");

                                    RaffleModel model = new RaffleModel();
                                    model.setDraw_id(object1.getString("draw_id"));
                                    model.setRecurring_draw_id(object1.getString("recurring_draw_id"));
                                    model.setDraw_uid(object1.getString("draw_uid"));
                                    model.setDraw_name(object1.getString("draw_name"));
                                    model.setDraw_type(object1.getString("draw_type"));
                                    model.setStatus(object1.getString("status"));
                                    model.setDraw_datetime(object1.getString("draw_datetime"));

                                    ArrayList<WinnersModel> winnersArrayList = new ArrayList<>();

                                    for (int j = 0; j < object1.getJSONArray("winners").length(); j++){
                                        JSONObject object2 = object1.getJSONArray("winners").getJSONObject(j);

                                        WinnersModel model1 = new WinnersModel();
                                        model1.setRaffle_uid(object2.getString("raffle_uid"));
                                        model1.setPrize_title(object2.getString("prize_title"));
                                        model1.setFirst_name(object2.getString("first_name"));
                                        model1.setPrize_number(object2.getString("prize_number"));

                                        winnersArrayList.add(model1);
                                    }

                                    model.setWinnersArrayList(winnersArrayList);
                                    raffleModelArrayList.add(model);
                                }

                                adapter_our_winners.notifyDataSetChanged();
                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getActivity(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
         //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }



}