package com.vouchers.mabroook.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Brand_Detail;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Activity.Search;
import com.vouchers.mabroook.Adapter.Adapter_latest_brands;
import com.vouchers.mabroook.Adapter.Adapter_more_tokens;
import com.vouchers.mabroook.Adapter.Adapter_rewards_category;
import com.vouchers.mabroook.Model.BrandModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Main_Bag_Frag extends Fragment {

    private RecyclerView recCategories;
    private Adapter_rewards_category adapter_rewards_category;

    private RecyclerView rec_More_Tokens, rec_Latest_Brands;
    private Adapter_more_tokens adapter_more_tokens;
    private Adapter_latest_brands adapter_latest_brands;

    private TextView recommended_TV, latest_TV;

    private UserSession session;

    private ArrayList<BrandModel> typeBrandArrayList = new ArrayList<>();

    private ArrayList<BrandModel> recommendArrayList = new ArrayList<>();
    private ArrayList<BrandModel> latestBrandArrayList = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_bag, container, false);

        session = new UserSession(getContext());


        String catName[] = {view.getContext().getString(R.string.all), view.getContext().getString(R.string.entertainment),
                view.getContext().getString(R.string.wellbeing), view.getContext().getString(R.string.food)};


        recCategories = view.findViewById(R.id.recCategories);
        rec_More_Tokens = view.findViewById(R.id.rec_More_Tokens);
        rec_Latest_Brands = view.findViewById(R.id.rec_Latest_Brands);
        recommended_TV = view.findViewById(R.id.recommended_TV);
        latest_TV = view.findViewById(R.id.latest_TV);



        view.findViewById(R.id.searchIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Search.class));
            }
        });



        recCategories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter_rewards_category = new Adapter_rewards_category(getContext(), typeBrandArrayList, new Adapter_rewards_category.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                if (item == 0){
                    recommended_TV.setVisibility(View.VISIBLE);
                    latest_TV.setVisibility(View.VISIBLE);
                    rec_More_Tokens.setVisibility(View.VISIBLE);

                } else if (item == 1){
                    recommended_TV.setVisibility(View.GONE);
                    latest_TV.setVisibility(View.GONE);
                    rec_More_Tokens.setVisibility(View.GONE);
                }

                getBrandList(typeBrandArrayList.get(item).getBrand_category_id());

            }
        });
        recCategories.setAdapter(adapter_rewards_category);



        rec_More_Tokens.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter_more_tokens = new Adapter_more_tokens(getContext(), recommendArrayList, new Adapter_more_tokens.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getContext(), Brand_Detail.class)
                        .putExtra("brand_id", recommendArrayList.get(item).getBrand_id()));
            }
        });
        rec_More_Tokens.setAdapter(adapter_more_tokens);



        rec_Latest_Brands.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_latest_brands = new Adapter_latest_brands(getContext(), latestBrandArrayList, new Adapter_latest_brands.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getContext(), Brand_Detail.class)
                        .putExtra("brand_id", latestBrandArrayList.get(item).getBrand_id()));
            }
        });
        rec_Latest_Brands.setAdapter(adapter_latest_brands);




        getBrandCat();

        return view;
    }



    private void getBrandCat() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-brand-categories",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("catBrandResponse", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray array = jsonObject.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++){
                                    JSONObject object = array.getJSONObject(i);

                                    BrandModel model = new BrandModel();
                                    model.setBrand_category_id(object.getString("brand_category_id"));
                                    model.setBrand_category_name(object.getString("brand_category_name"));
                                    model.setBrand_category_icon_white(object.getString("brand_category_icon_white"));

                                    typeBrandArrayList.add(model);
                                }

                                adapter_rewards_category.notifyDataSetChanged();
                            }

                            getBrandList(typeBrandArrayList.get(0).getBrand_category_id());


                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


    private void getBrandList(String cat_ID) {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-brands?brand_category_id=" + cat_ID,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        recommendArrayList.clear();
                        latestBrandArrayList.clear();

                        try {
                            Log.e("ListBrandResponse", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                JSONArray array = object.getJSONArray("recommended_brands");

                                for (int i = 0; i < array.length(); i++){
                                    JSONObject object1 = array.getJSONObject(i);

                                    BrandModel model = new BrandModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setBrand_category_id(object1.getString("brand_category_id"));
                                    model.setBrand_name(object1.getString("brand_name"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setTag_line(object1.getString("tag_line"));
                                    model.setBackground_image(object1.getString("background_image"));

                                    recommendArrayList.add(model);
                                }


                                JSONArray array1 = object.getJSONArray("latest_brands");

                                for (int i = 0; i < array1.length(); i++){
                                    JSONObject object1 = array1.getJSONObject(i);

                                    BrandModel model = new BrandModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setBrand_category_id(object1.getString("brand_category_id"));
                                    model.setBrand_name(object1.getString("brand_name"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setTag_line(object1.getString("tag_line"));
                                    model.setBackground_image(object1.getString("background_image"));

                                    latestBrandArrayList.add(model);
                                }

                                adapter_more_tokens.notifyDataSetChanged();
                                adapter_latest_brands.notifyDataSetChanged();
                            }


                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }




}