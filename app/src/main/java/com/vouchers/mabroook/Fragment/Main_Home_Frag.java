package com.vouchers.mabroook.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.vouchers.mabroook.Activity.Activate_Tokens;
import com.vouchers.mabroook.Activity.Barcode_Scanner;
import com.vouchers.mabroook.Activity.Brand_Detail;
import com.vouchers.mabroook.Activity.Login_Signup.Login_SignUp;
import com.vouchers.mabroook.Activity.Profile;
import com.vouchers.mabroook.Activity.Your_Token;
import com.vouchers.mabroook.Adapter.Adapter_active_tokens;
import com.vouchers.mabroook.Adapter.Adapter_more_tokens;
import com.vouchers.mabroook.Adapter.Adapter_recently_active_tokens;
import com.vouchers.mabroook.Model.ActivateModel;
import com.vouchers.mabroook.Model.BrandModel;
import com.vouchers.mabroook.Model.RecentlyActivateModel;
import com.vouchers.mabroook.R;
import com.vouchers.mabroook.Utils.UserSession;
import com.vouchers.mabroook.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Main_Home_Frag extends Fragment {

    private UserSession session;
    private Adapter_more_tokens adapter_more_tokens;
    private Adapter_active_tokens adapter_active_tokens;
    private Adapter_recently_active_tokens adapter_recently_active_tokens;
    private RecyclerView rec_More_Tokens, rec_Active_Tokens, rec_Recently_Active_Tokens;

    private TextView how_this_work_TV;

    private TextView active_tokens, inactive_tokens, prev_win_date;

    private ImageView prev_image_1, prev_image_2, prev_image_3, prev_image_4, prev_image_5, profile_pic;
    private ImageView next_image_1, next_image_2, next_image_3, next_image_4, next_image_5;

    private TextView prev_prize_1, prev_prize_2, prev_prize_3, prev_prize_4, prev_prize_5;
    private TextView next_prize_1, next_prize_2, next_prize_3, next_prize_4, next_prize_5;

    private TextView prev_person_name_1, prev_person_name_2, prev_person_name_3, prev_person_name_4, prev_person_name_5;
    private TextView next_person_name_4, next_person_name_5;

    private LinearLayout prev_linear_4, prev_linear_5;
    private LinearLayout next_linear_4, next_linear_5;

    private ProgressBar progressBar, progressRaffle;
    private TextView next_raffle_timer, special_draw_timer, special_draw_name;

    private ArrayList<BrandModel> recommendArrayList = new ArrayList<>();
    private ArrayList<ActivateModel> activateArrayList = new ArrayList<>();
    private ArrayList<RecentlyActivateModel> recentlyActivateArrayList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        session = new UserSession(getContext());

        Log.e("token", session.getAPITOKEN() + "--");


        rec_More_Tokens = view.findViewById(R.id.rec_More_Tokens);
        rec_Active_Tokens = view.findViewById(R.id.rec_Active_Tokens);
        rec_Recently_Active_Tokens = view.findViewById(R.id.rec_Recently_Active_Tokens);
        how_this_work_TV = view.findViewById(R.id.how_this_work_TV);

        active_tokens = view.findViewById(R.id.active_tokens);
        inactive_tokens = view.findViewById(R.id.inactive_tokens);
        prev_win_date = view.findViewById(R.id.prev_win_date);

        prev_image_1 = view.findViewById(R.id.prev_image_1);
        prev_image_2 = view.findViewById(R.id.prev_image_2);
        prev_image_3 = view.findViewById(R.id.prev_image_3);
        prev_image_4 = view.findViewById(R.id.prev_image_4);
        prev_image_5 = view.findViewById(R.id.prev_image_5);
        next_image_1 = view.findViewById(R.id.next_image_1);
        next_image_2 = view.findViewById(R.id.next_image_2);
        next_image_3 = view.findViewById(R.id.next_image_3);
        next_image_4 = view.findViewById(R.id.next_image_4);
        next_image_5 = view.findViewById(R.id.next_image_5);

        prev_prize_1 = view.findViewById(R.id.prev_prize_1);
        prev_prize_2 = view.findViewById(R.id.prev_prize_2);
        prev_prize_3 = view.findViewById(R.id.prev_prize_3);
        prev_prize_4 = view.findViewById(R.id.prev_prize_4);
        prev_prize_5 = view.findViewById(R.id.prev_prize_5);
        next_prize_1 = view.findViewById(R.id.next_prize_1);
        next_prize_2 = view.findViewById(R.id.next_prize_2);
        next_prize_3 = view.findViewById(R.id.next_prize_3);
        next_prize_4 = view.findViewById(R.id.next_prize_4);
        next_prize_5 = view.findViewById(R.id.next_prize_5);

        prev_person_name_1 = view.findViewById(R.id.prev_person_name_1);
        prev_person_name_2 = view.findViewById(R.id.prev_person_name_2);
        prev_person_name_3 = view.findViewById(R.id.prev_person_name_3);
        prev_person_name_4 = view.findViewById(R.id.prev_person_name_4);
        prev_person_name_5 = view.findViewById(R.id.prev_person_name_5);
        next_person_name_4 = view.findViewById(R.id.next_person_name_4);
        next_person_name_5 = view.findViewById(R.id.next_person_name_5);

        prev_linear_4 = view.findViewById(R.id.prev_linear_4);
        prev_linear_5 = view.findViewById(R.id.prev_linear_5);
        next_linear_4 = view.findViewById(R.id.next_linear_4);
        next_linear_5 = view.findViewById(R.id.next_linear_5);

        next_raffle_timer = view.findViewById(R.id.next_raffle_timer);
        special_draw_timer = view.findViewById(R.id.special_draw_timer);
        special_draw_name = view.findViewById(R.id.special_draw_name);
        profile_pic = view.findViewById(R.id.profile_pic);


        how_this_work_TV.setPaintFlags(how_this_work_TV.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        progressBar = view.findViewById(R.id.progress);
        progressRaffle = view.findViewById(R.id.progressRaffle);



        view.findViewById(R.id.profileLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Profile.class));
            }
        });

        view.findViewById(R.id.your_tokens).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Your_Token.class));
            }
        });

        view.findViewById(R.id.btnMyTickets).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Your_Token.class));
            }
        });

        view.findViewById(R.id.scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED){
                    checkAndroidVersion();
                } else {
                    startActivity(new Intent(getContext(), Barcode_Scanner.class));
                }
            }
        });




        rec_More_Tokens.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter_more_tokens = new Adapter_more_tokens(getContext(), recommendArrayList, new Adapter_more_tokens.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getContext(), Brand_Detail.class)
                        .putExtra("brand_id", recommendArrayList.get(item).getBrand_id()));
            }
        });
        rec_More_Tokens.setAdapter(adapter_more_tokens);


        rec_Active_Tokens.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_active_tokens = new Adapter_active_tokens(getContext(), activateArrayList, new Adapter_active_tokens.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getContext(), Activate_Tokens.class)
                        .putExtra("partner_id", activateArrayList.get(item).getPartner_id())
                        .putExtra("voucher_cnt", activateArrayList.get(item).getVoucher_cnt()));
            }
        });
        rec_Active_Tokens.setAdapter(adapter_active_tokens);


        rec_Recently_Active_Tokens.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_recently_active_tokens = new Adapter_recently_active_tokens(getContext(), recentlyActivateArrayList, new Adapter_recently_active_tokens.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }

            @Override
            public void onItemClaimWon(int item) {

            }
        });
        rec_Recently_Active_Tokens.setAdapter(adapter_recently_active_tokens);


        return view;
    }



    private void getHomeDetail() {
        KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "home-screen-details",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        recommendArrayList.clear();
                        activateArrayList.clear();
                        recentlyActivateArrayList.clear();

                        try {
                            Log.e("home_detail_response", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                Glide.with(getContext()).load(object.getString("profile_pic")).into(profile_pic);
                                active_tokens.setText(object.getString("total_active_tokens"));
                                inactive_tokens.setText(object.getString("total_inactive_tokens"));
                                prev_win_date.setText(object.getString("previous_winners_draw_datetime"));


                                JSONArray array = object.getJSONArray("previous_winners");

                                prev_prize_1.setText(array.getJSONObject(0).getString("prize_title"));
                                prev_prize_2.setText(array.getJSONObject(1).getString("prize_title"));
                                prev_prize_3.setText(array.getJSONObject(2).getString("prize_title"));
                                if (array.length() > 3) {
                                    prev_prize_4.setText(array.getJSONObject(3).getString("prize_title"));
                                    prev_linear_4.setVisibility(View.VISIBLE);
                                    if (array.length() > 4) {
                                        prev_prize_5.setText(array.getJSONObject(4).getString("prize_title"));
                                        prev_linear_5.setVisibility(View.VISIBLE);
                                    }
                                }

                                prev_person_name_1.setText(array.getJSONObject(0).getString("first_name"));
                                prev_person_name_2.setText(array.getJSONObject(1).getString("first_name"));
                                prev_person_name_3.setText(array.getJSONObject(2).getString("first_name"));
                                if (array.length() > 3) {
                                    prev_person_name_4.setText(array.getJSONObject(3).getString("first_name"));
                                    if (array.length() > 4) {
                                        prev_person_name_5.setText(array.getJSONObject(4).getString("first_name"));
                                    }
                                }

                                Glide.with(getContext()).load(array.getJSONObject(0).getString("prize_image")).into(prev_image_1);
                                Glide.with(getContext()).load(array.getJSONObject(1).getString("prize_image")).into(prev_image_2);
                                Glide.with(getContext()).load(array.getJSONObject(2).getString("prize_image")).into(prev_image_3);
                                if (array.length() > 3) {
                                    Glide.with(getContext()).load(array.getJSONObject(3).getString("prize_image")).into(prev_image_4);
                                    if (array.length() > 4) {
                                        Glide.with(getContext()).load(array.getJSONObject(4).getString("prize_image")).into(prev_image_5);
                                    }
                                }



                                JSONArray array1 = object.getJSONObject("next_raffle").getJSONArray("prize_list");

                                next_prize_1.setText(array1.getJSONObject(0).getString("prize_title"));
                                next_prize_2.setText(array1.getJSONObject(1).getString("prize_title"));
                                next_prize_3.setText(array1.getJSONObject(2).getString("prize_title"));
                                if (array1.length() > 3) {
                                    next_prize_4.setText(array1.getJSONObject(3).getString("prize_amount"));
                                    next_linear_4.setVisibility(View.VISIBLE);
                                    if (array1.length() > 4) {
                                        next_prize_5.setText(array1.getJSONObject(4).getString("prize_amount"));
                                        next_linear_5.setVisibility(View.VISIBLE);
                                    }
                                }


                                if (array1.length() > 3) {
                                    next_person_name_4.setText(array1.getJSONObject(3).getString("prize_title"));
                                    if (array1.length() > 4) {
                                        next_person_name_5.setText(array1.getJSONObject(4).getString("prize_title"));
                                    }
                                }

                                Glide.with(getContext()).load(array1.getJSONObject(0).getString("prize_image")).into(next_image_1);
                                Glide.with(getContext()).load(array1.getJSONObject(1).getString("prize_image")).into(next_image_2);
                                Glide.with(getContext()).load(array1.getJSONObject(2).getString("prize_image")).into(next_image_3);
                                if (array1.length() > 3) {
                                    Glide.with(getContext()).load(array1.getJSONObject(3).getString("prize_image")).into(next_image_4);
                                    if (array1.length() > 4) {
                                        Glide.with(getContext()).load(array1.getJSONObject(4).getString("prize_image")).into(next_image_5);
                                    }
                                }



                                // FOR DRAW TIMER AND PROGRESS BAR
                                String dateStart = object.getJSONObject("next_raffle").getString("created_at");
                                String dateEnd = object.getJSONObject("next_raffle").getString("draw_datetime");

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date dateSt = null;
                                Date dateEn = null;
                                try {
                                    dateSt = sdf.parse(dateStart);
                                    dateEn = sdf.parse(dateEnd);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long startDate = dateSt.getTime();
                                long endDate = dateEn.getTime();

                                long diffDate = TimeUnit.HOURS.toHours(endDate - startDate)/(60*60*1000);

                                Log.e("fdff", diffDate + "--" + dateEnd);

                                Date c = Calendar.getInstance().getTime();
                                long currentDate = c.getTime();

                                long diffDate1 = TimeUnit.HOURS.toHours(currentDate - startDate)/(60*60*1000);

                                Log.e("getTimeInfo", diffDate1 + "--");

                                long currentDiff = diffDate - diffDate1;


                                float progress = 100 - ((float)currentDiff/diffDate * 100);

                                progressBar.setProgress((int)progress);
                                progressBar.setScaleY(1.5f);
                                progressBar.getProgressDrawable().setColorFilter(getResources().getColor(R.color.orange3), android.graphics.PorterDuff.Mode.SRC_IN);



                                // FOR DRAW TIMER AND PROGRESS BAR
                                special_draw_name.setText(object.getJSONObject("special_draw").getString("draw_name"));

                                String dateStart1 = object.getJSONObject("special_draw").getString("created_at");
                                String dateEnd1 = object.getJSONObject("special_draw").getString("draw_datetime");

                                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date dateSt1 = null;
                                Date dateEn1 = null;
                                try {
                                    dateSt1 = sdf1.parse(dateStart1);
                                    dateEn1 = sdf1.parse(dateEnd1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long startDate1 = dateSt1.getTime();
                                long endDate1 = dateEn1.getTime();

                                long diffDate2 = TimeUnit.HOURS.toHours(endDate1 - startDate1)/(60*60*1000);

                                long currentDiff1 = diffDate2 - diffDate1;


                                float progress1 = 100 - ((float)currentDiff1/diffDate2 * 100);

                                progressRaffle.setProgress((int)progress1);
                                progressRaffle.setScaleY(1.6f);
                                progressRaffle.getProgressDrawable().setColorFilter(getResources().getColor(R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);


                                setDrawTimer(TimeUnit.HOURS.toHours(endDate - currentDate), TimeUnit.HOURS.toHours(endDate1 - currentDate));


                                JSONArray array2 = object.getJSONArray("recommended_brands");
                                for (int i = 0; i < array2.length(); i++){
                                    JSONObject object1 = array2.getJSONObject(i);

                                    BrandModel model = new BrandModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setBrand_category_id(object1.getString("brand_category_id"));
                                    model.setBrand_name(object1.getString("brand_name"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setTag_line(object1.getString("tag_line"));
                                    model.setBackground_image(object1.getString("background_image"));
                                    recommendArrayList.add(model);
                                }

                                JSONArray array3 = object.getJSONArray("activate_tokens");
                                for (int i = 0; i < array3.length(); i++){
                                    JSONObject object1 = array3.getJSONObject(i);

                                    ActivateModel model = new ActivateModel();
                                    model.setBrand_id(object1.getString("brand_id"));
                                    model.setPartner_name(object1.getString("partner_name"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setBrand_name(object1.getString("brand_name"));
                                    model.setBackground_color(object1.getString("background_color"));
                                    model.setVoucher_cnt(object1.getString("voucher_cnt"));
                                    model.setPartner_id(object1.getString("partner_id"));
                                    activateArrayList.add(model);
                                }

                                JSONArray array4 = object.getJSONArray("recently_activated_tokens");
                                for (int i = 0; i < array4.length(); i++){
                                    JSONObject object1 = array4.getJSONObject(i);

                                    RecentlyActivateModel model = new RecentlyActivateModel();
                                    model.setVoucher_id(object1.getString("voucher_id"));
                                    model.setRaffle_id(object1.getString("raffle_id"));
                                    model.setVoucher_uid(object1.getString("voucher_uid"));
                                    model.setRaffle_uid(object1.getString("raffle_uid"));
                                    model.setBrand_logo(object1.getString("brand_logo"));
                                    model.setRaffle_status(object1.getString("raffle_status"));
                                    model.setActivation_date(object1.getString("activation_date"));
                                    model.setMultiple_draw_name(object1.getString("multiple_draw_name"));
                                    model.setRaffle_cnt(object1.getString("raffle_cnt"));
                                    model.setDraw_datetime(object1.getString("draw_datetime"));
                                    model.setVoucher_status(object1.getString("voucher_status"));
                                    recentlyActivateArrayList.add(model);
                                }

                                adapter_more_tokens.notifyDataSetChanged();
                                adapter_active_tokens.notifyDataSetChanged();
                                adapter_recently_active_tokens.notifyDataSetChanged();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        String json1 = new String(error.networkResponse.data);
                        json1 = session.trimMessage(json1, "message");
                        if(json1 != null) {

                            if (error.networkResponse.statusCode == 401) {
                                Toast.makeText(getContext(), json1, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getContext(), Login_SignUp.class));
                                getActivity().finishAffinity();
                                session.logout();
                            }
                        }

                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_networ_connection, Toast.LENGTH_LONG).show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //       params.put("email_phone", email_ET.getText().toString().trim());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }



    private void setDrawTimer(long duration, long duration_spec_draw) {

        Log.e("sfdff", duration + "--");

        new CountDownTimer(duration, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds

                String hms = String.format("%02d days  :  %02d hours  :  %02d minutes",
                        TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millis)),
                        (TimeUnit.MILLISECONDS.toHours(millis) -
                                TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis))),
                        (TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))));

                next_raffle_timer.setText(hms);
            }
            public void onFinish() {
                next_raffle_timer.setText("Raffle finish!");
            }
        }.start();


        new CountDownTimer(duration_spec_draw, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds

                String hms = String.format("%02d days  :  %02d hours  :  %02d minutes",
                        TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millis)),
                        (TimeUnit.MILLISECONDS.toHours(millis) -
                                TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis))),
                        (TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))));

                special_draw_timer.setText(hms);
            }
            public void onFinish() {
                special_draw_timer.setText("finish!");
            }
        }.start();

    }


    private void checkAndroidVersion() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }
    }


    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            // ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }


    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Permision_done","Permision_done");
        switch (requestCode) {

            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();

                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);

                    Log.e("Permision_done","Permision_done");

                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");


                        startActivity(new Intent(getContext(), Barcode_Scanner.class));

                        Log.e("Permision_done","Permision_done");

                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");

                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                            showDialogOK(getString(R.string.camera_storegae_required),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    break;
                                            }
                                        }
                                    });
                        }

                        else {
                            Toast.makeText(getContext(), getString(R.string.go_to_setting_permission), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                }
            }
        }

    }


    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(R.string.ok, okListener)
                .setNegativeButton(R.string.cancel, okListener)
                .create()
                .show();
    }


    @Override
    public void onResume() {
        super.onResume();

        getHomeDetail();
    }
}